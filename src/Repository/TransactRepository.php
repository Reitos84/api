<?php

namespace App\Repository;

use App\Entity\Transact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Transact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transact[]    findAll()
 * @method Transact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Transact::class);
    }

    // /**
    //  * @return Transact[] Returns an array of Transact objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transact
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
