<?php

namespace App\Repository;

use App\Entity\CampainAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CampainAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampainAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampainAction[]    findAll()
 * @method CampainAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampainActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampainAction::class);
    }

    // /**
    //  * @return CampainAction[] Returns an array of CampainAction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CampainAction
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
