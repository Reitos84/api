<?php

namespace App\Repository;

use App\Entity\CampainType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CampainType|null find($id, $lockMode = null, $lockVersion = null)
 * @method CampainType|null findOneBy(array $criteria, array $orderBy = null)
 * @method CampainType[]    findAll()
 * @method CampainType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampainTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CampainType::class);
    }

    // /**
    //  * @return CampainType[] Returns an array of CampainType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CampainType
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
