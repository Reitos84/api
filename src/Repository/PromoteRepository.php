<?php

namespace App\Repository;

use App\Entity\Promote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Promote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Promote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Promote[]    findAll()
 * @method Promote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PromoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Promote::class);
    }

    // /**
    //  * @return Promote[] Returns an array of Promote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Promote
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
