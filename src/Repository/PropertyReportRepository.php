<?php

namespace App\Repository;

use App\Entity\PropertyReport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyReport[]    findAll()
 * @method PropertyReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyReport::class);
    }

    // /**
    //  * @return PropertyReport[] Returns an array of PropertyReport objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PropertyReport
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
