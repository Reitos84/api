<?php

namespace App\Repository;

use App\Entity\PropertyAlerte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PropertyAlerte|null find($id, $lockMode = null, $lockVersion = null)
 * @method PropertyAlerte|null findOneBy(array $criteria, array $orderBy = null)
 * @method PropertyAlerte[]    findAll()
 * @method PropertyAlerte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PropertyAlerteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PropertyAlerte::class);
    }

    // /**
    //  * @return PropertyAlerte[] Returns an array of PropertyAlerte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PropertyAlerte
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
