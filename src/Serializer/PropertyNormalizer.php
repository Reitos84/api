<?php

namespace App\Serializer;

use App\Entity\Property;
use App\Entity\Saved;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class PropertyNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{

    use NormalizerAwareTrait;

    private const ALREADY_CALLED_PROPERTY_NORMALIZER = 'AppPropertyNormalizerAlreadyCalled';
    private $storage;
    private $security;
    private $manager;

    public function __construct(StorageInterface $store, Security $security, EntityManagerInterface $manager)
    {
        $this->storage = $store;
        $this->security = $security;
        $this->manager = $manager;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_PROPERTY_NORMALIZER]) && $data instanceof Property;
    }

    /**
     * @param Property $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $user = $this->security->getUser();
        if($user){
            $savedRepository = $this->manager->getRepository(Saved::class);
            $mySave = $savedRepository->findOneBy(['client'=>$user, 'property'=>$object]);
            if($mySave){
                $object->setISaved($mySave->getId());
            }else{
                $object->setISaved(0);
            }
        }
        $context[self::ALREADY_CALLED_PROPERTY_NORMALIZER] = true;
        return $this->normalizer->normalize($object, $format, $context);
    }
}