<?php

namespace App\Serializer;

use App\Entity\PropertyType;
use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class PropertyTypeNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{

    use NormalizerAwareTrait;

    private const ALREADY_CALLED_TYPE_NORMALIZER = 'AppTypeNormalizerAlreadyCalled';
    private $storage;
    private $manager;

    public function __construct(StorageInterface $store, EntityManagerInterface $manager)
    {
        $this->storage = $store;
        $this->manager = $manager;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_TYPE_NORMALIZER]) && $data instanceof PropertyType;
    }

    /**
     * @param PropertyType $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED_TYPE_NORMALIZER] = true;
        $object->setIcon('/images/type/' . $object->getIcon());
        return $this->normalizer->normalize($object, $format, $context);
    }
}