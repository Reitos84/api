<?php

namespace App\Serializer;

use App\Entity\Property;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class UserNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{

    use NormalizerAwareTrait;

    private const ALREADY_CALLED_USER_NORMALIZER = 'AppUserNormalizerAlreadyCalled';
    private $storage;
    private $manager;

    public function __construct(StorageInterface $store, EntityManagerInterface $manager)
    {
        $this->storage = $store;
        $this->manager = $manager;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_USER_NORMALIZER]) && $data instanceof User;
    }

    /**
     * @param User $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $object->setAvatarUrl($this->storage->resolveUri($object, 'file'));
        $context[self::ALREADY_CALLED_USER_NORMALIZER] = true;
        if(isset($context['request_uri']) && preg_match('/ROLE_AGENCY/', $context['request_uri'])){
            $agencyRepo = $this->manager->getRepository(Property::class);
            $object->setPropertyTotal($agencyRepo->count(['agency' => $object]));
        }
        return $this->normalizer->normalize($object, $format, $context);
    }
}