<?php

namespace App\Serializer;

use App\Entity\Picture;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class PictureNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{

    use NormalizerAwareTrait;

    private const ALREADY_CALLED_PICTURE_NORMALIZER = 'AppPictureNormalizerAlreadyCalled';
    private $storage;

    public function __construct(StorageInterface $store)
    {
        $this->storage = $store;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_PICTURE_NORMALIZER]) && $data instanceof Picture;
    }

    /**
     * @param Picture $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $object->setUrl($this->storage->resolveUri($object, 'file'));
        $object->setThumbnail('/images/property/' . $object->getThumbnail());
        $context[self::ALREADY_CALLED_PICTURE_NORMALIZER] = true;
        return $this->normalizer->normalize($object, $format, $context);
    }
}