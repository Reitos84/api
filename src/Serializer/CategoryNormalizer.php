<?php

namespace App\Serializer;

use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class CategoryNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{

    use NormalizerAwareTrait;

    private const ALREADY_CALLED_CATEGORY_NORMALIZER = 'AppCategoryNormalizerAlreadyCalled';
    private $storage;
    private $manager;

    public function __construct(StorageInterface $store, EntityManagerInterface $manager)
    {
        $this->storage = $store;
        $this->manager = $manager;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_CATEGORY_NORMALIZER]) && $data instanceof Category;
    }

    /**
     * @param Category $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED_CATEGORY_NORMALIZER] = true;
        $object->setIcon('/images/category/' . $object->getIcon());
        return $this->normalizer->normalize($object, $format, $context);
    }
}