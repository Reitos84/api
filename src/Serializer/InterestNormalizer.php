<?php

namespace App\Serializer;

use App\Entity\Interest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;

class InterestNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface {
    
    use NormalizerAwareTrait;

    private const ALREADY_CALLED_INTEREST_NORMALIZER = 'AppInterestNormalizerAlreadyCalled';
    private $_manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->_manager = $manager;
    }

    public function supportsNormalization($data, ?string $format = null, array $context = [])
    {
        return !isset($context[self::ALREADY_CALLED_INTEREST_NORMALIZER]) && $data instanceof Interest;
    }

    /**
     * @param Interest $object
     */
    public function normalize($object, ?string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED_INTEREST_NORMALIZER] = true;
        $now = new \DateTime();
        $date_diff = date_diff($now, $object->getCreatedAt());
        if($date_diff->days >= 3){
            $object->setIsExpired(true);
        }
        return $this->normalizer->normalize($object, $format, $context);
    }
}