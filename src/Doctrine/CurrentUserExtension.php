<?php

namespace App\Doctrine;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\AgencyOwnedInterface;
use App\Entity\Property;
use App\Entity\UserOwnedInterface;
use Symfony\Component\Security\Core\Security;

class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{

    private $security;

    public function __construct(Security $secure)
    {   
        $this->security = $secure;
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder, 
        QueryNameGeneratorInterface $queryNameGenerator, 
        string $resourceClass, 
        ?string $operationName = null
    ){
        $this->addWhere($resourceClass, $queryBuilder);
    }

    public function applyToItem(
        QueryBuilder $queryBuilder, 
        QueryNameGeneratorInterface $queryNameGenerator, 
        string $resourceClass, 
        array $identifiers, 
        ?string $operationName = null, 
        array $context = []
    ){
        $this->addWhere($resourceClass, $queryBuilder);
    }

    public function addWhere(String $resourceClass, QueryBuilder $queryBuilder){
        $user = $this->security->getUser();
        if($user && in_array('ROLE_AGENCY', $user->getRoles())){
            $reflectionClass = new \ReflectionClass($resourceClass);
            if($reflectionClass->implementsInterface(AgencyOwnedInterface::class)){
                $alias = $queryBuilder->getRootAlias()[0];
                $queryBuilder
                        ->andWhere("$alias.agency = :connected_current_user")
                        ->setParameter("connected_current_user", $user->getId());
            }
            // if($reflectionClass->implementsInterface(UserOwnedInterface::class)){
            //     dd($user);
            //     $alias = $queryBuilder->getRootAlias()[0];
            //     $queryBuilder
            //             ->andWhere("$alias.user = :connected_current_user")
            //             ->setParameter("connected_current_user", $user->getId());
            // }
        }
    }
}