<?php

namespace App\EventListener;

use App\Entity\Property;
use App\Service\NotificationService;
use App\Service\SendGridMailerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class SavePropertyListener
{
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    private $mailer;
    private $manager;
    private $notifier;

    public function __construct(SendGridMailerService $mailer, EntityManagerInterface $manager, NotificationService $notifier)
    {
        $this->mailer = $mailer;
        $this->manager = $manager;
        $this->notifier = $notifier;
    }

    public function __invoke(Property $property, LifecycleEventArgs $event): void
    {
        /* write your logic here */
        // $this->mailer->clientRegisteredMail($user);
        $repository = $this->manager->getRepository(Property::class);
        $user = $property->getAgency();
        $currentUserProperty = $repository->count(['agency' => $user]);
        if($currentUserProperty === 1){
            try{
                $this->mailer->sendFirstProperty($user, $property);
            }catch(\Throwable $t){

            }
            $this->notifier->sendFirstPropertyRegister($user, $property);
        }else{
            $this->notifier->registerProperty($user, $property);
        }
    }
}