<?php
namespace App\EventListener;

use App\Entity\User;
use App\Service\SendGridMailerService;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;

class EditUserListener
{
    private $mailer;
    private $user;

    public function __construct(SendGridMailerService $mailer, Security $security)
    {
        $this->mailer = $mailer;
        $this->user = $security->getUser();
    }

    public function postUpdate(User $user, LifecycleEventArgs $event){
        //$user->setEmail('client5mille@gmail.com');
        //$this->mailer->agencyRequestEmail($user);
    }
}