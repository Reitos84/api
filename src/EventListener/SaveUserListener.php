<?php

namespace App\EventListener;

use App\Entity\User;
use App\Service\SendGridMailerService;
use App\Service\TwilioService;
use App\Service\VonageService;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class SaveUserListener
{
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    private $mailer;
    private $twilioService;

    public function __construct(SendGridMailerService $mailer, TwilioService $twilioService)
    {
        $this->mailer = $mailer;
        $this->twilioService = $twilioService;
    }

    public function __invoke(User $user, LifecycleEventArgs $event): void
    {
        /* write your logic here */
        try{
            if(in_array('ROLE_AGENCY', $user->getRoles())){
                if($user->getIsSignAsAgency()){
                    $this->mailer->agencyRequestEmail($user);
                    $this->twilioService->sendSms('2250507369500', 'Vous avez une requête d\'inscription pour une agence.');
                }
            }else{
                $this->mailer->clientRegisteredMail($user);
            }
        }catch(\Throwable $t){
            
        }
    }
}