<?php

namespace App\EventListener;

use App\Entity\User;
use App\Service\SendGridMailerService;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class SaveBookingListener
{
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    private $mailer;

    public function __construct(SendGridMailerService $mailer)
    {
        $this->mailer = $mailer;
    }

    public function __invoke(User $user, LifecycleEventArgs $event): void
    {
        /* write your logic here */
        
    }
}