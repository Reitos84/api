<?php

namespace App\EventListener;

use App\Entity\Interest;
use App\Service\SendGridMailerService;
use App\Service\VonageService;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class SaveInterestListener
{
    // the listener methods receive an argument which gives you access to
    // both the entity object of the event and the entity manager itself
    private $mailer;
    private $vonage;

    public function __construct(SendGridMailerService $mailer, VonageService $sms)
    {
        $this->mailer = $mailer;
        $this->vonage = $sms;
    }

    public function __invoke(Interest $interest, LifecycleEventArgs $event): void
    {
        /* write your logic here */
        $agency = $interest->getAgency();
        try{
            $this->mailer->sendAddInterest($agency);
            $text = "Abidjan Habitat: Vous avez un nouvel intérêt. Consulter maintenant sur " . $_SERVER['AGENCY_LINK'] . ".";
            $this->vonage->sendSms($agency->getMobilephone(), $text);
        }catch(\Throwable $e){

        }
        //$this->notifier->sendInterestNotifier($agency);
    }
}