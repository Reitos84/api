<?php

namespace App\Service;

use \Osms\Osms;
use Symfony\Component\HttpClient\HttpClient;

class SmsService{

    private $clientId;
    private $clientSecret;
    private $token;
    private $acces_token;

    public function __construct($token)
    {
        $this->clientId = 'UxlfagBRjvcsGGkRtwK7bYWSIJ8Bh4Ph';
        $this->clientSecret = 'k8jKkM90YBFVlAWP';
        $this->token = $token;
    }

    public function login(){
        $client = HttpClient::create();
        try{
            $request = $client->request('POST', 'https://api.orange.com/oauth/v3/token', [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Basic VXhsZmFnQlJqdmNzR0drUnR3SzdiWVdTSUo4Qmg0UGg6azhqS2tNOTBZQkZWbEFXUA=='
                ],
                'body' => [
                    'grant_type' => 'client_credentials'
                ]
            ]);
            if(!$request->getContent()){
                return false;
            }
            $data = json_decode($request->getContent());
            if($data->token_type && $data->access_token){
                $this->acces_token = $data->token_type . ' ' . $data->access_token;
                return true;
            }else{
                return false;
            }
        }catch(\Throwable $t){
            return false;
        }
    }

    public function sendSms(String $phoneNumber, String $text){
        try{
            $client = HttpClient::create();

            $request = $client->request('POST', 'https://api.orange.com/smsmessaging/v1/outbound/tel:+2250769241051/requests', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $this->acces_token
                ],
                'json' => [
                    'outboundSMSMessageRequest' => [
                        'address' => "tel:$phoneNumber",
                        'senderAddress' => "tel:+2250769241051",
                        'outboundSMSTextMessage' => [
                            'message' => $text
                        ]
                    ]
                ]
            ]);

            return true;
        }catch(\Throwable $t){
            return false;
        }
    }
}