<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;

class VonageService
{

    public function __construct(){
    }

    public function sendSms(String $contact, String $text){
        $client = HttpClient::create();
        try{
            $request = $client->request('POST', 'https://rest.nexmo.com/sms/json?api_key='. $_SERVER['API_KEY'] . '&api_secret=' . $_SERVER['API_SECRET'], [
                'body' => [
                    'from' => '18335784387',
                    'to' => $contact,
                    'text' => $text
                ]
            ]);
            if(!$request->getContent()){
                return false;
            }
            $data = json_decode($request->getContent());
            return true;
        }catch(\Throwable $t){
            return false;
        }
    }
}