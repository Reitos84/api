<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Messenger\SendEmailMessage;

class SendGridMailerService {

    private $mailer;
    private $sender;

    public function __construct(MailerInterface $email)
    {
        $this->mailer = $email;
        $this->sender = new Address('support@abidjanhabitat.com', 'Serge depuis Abidjan Habitât');
    }

    public function clientRegisteredMail(User $user){

        $isAgency = in_array('ROLE_AGENCY', $user->getRoles());

        $email = (new TemplatedEmail())
            ->from($this->sender)
            ->to($user->getEmail())
            ->priority(Email::PRIORITY_NORMAL)
            ->subject('Bienvenue sur Abidjan Habitât')
            ->htmlTemplate('email/clientRegisterNew.html.twig')
            ->context([
                'company' => $_SERVER['COMPANY_NAME'],
                'name' => $user->getName(),
                'isAgency' => $isAgency,
                'agencylink' => $_SERVER['AGENCY_LINK'],
                'sitelink' => $_SERVER['CLIENT_LINK']
            ])
        ;
        $this->mailer->send($email);
    }

    public function sendAgencyAcceptRequest($user, $password){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to($user->getEmail())
        ->priority(Email::PRIORITY_NORMAL)
        ->subject('Bienvenue sur notre reseau immobilier')
        ->htmlTemplate('email/acceptAgency.html.twig')
        ->context([
            'name' => $user->getName(),
            'supportEmail' => $_SERVER['AGENCY_SUPPORT'],
            'username' => $user->getEmail(),
            'agencyLink' => $_SERVER['AGENCY_LINK'],
            'password' => $password,
        ]);

        $this->mailer->send($email);
    }

    public function agencyRequestEmail($user){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to($user->getEmail())
        ->priority(Email::PRIORITY_NORMAL)
        ->subject('Adhérer au réseau immobilier')
        ->htmlTemplate('email/agencyRequest.html.twig')
        ->context([
            'name' => $user->getName(),
            'supportEmail' => $_SERVER['COMPANY_EMAIL'],
        ]);

        $this->mailer->send($email);
    }

    public function sendFirstProperty($user, $property){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to($user->getEmail())
        ->priority(Email::PRIORITY_NORMAL)
        ->subject('Publication réussie !')
        ->htmlTemplate('email/firstproperty.html.twig')
        ->context([
            'company' => $_SERVER['COMPANY_NAME'],
            'name' => $user->getName()
        ]);

        $this->mailer->send($email);
    }

    public function notifNewAgency(){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to("somuchapp@gmail.com")
        ->priority(Email::PRIORITY_NORMAL)
        ->subject('Nouvelle demande d\'agence')
        ->htmlTemplate('email/newAgency.html.twig')
        ->context([
            ''
        ]);

        $this->mailer->send($email);
    }


    public function sendAddInterest($agency){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to($agency->getEmail())
        ->priority(Email::PRIORITY_NORMAL)
        ->subject('Vous avez un nouvel intérêt')
        ->htmlTemplate('email/interest.html.twig')
        ->context([
            'agencylink' => $_SERVER['AGENCY_LINK'],
            'name' => $agency->getName()
        ]);

        $this->mailer->send($email);
    }

    public function twoFactor($account, $code, $name){
        $email = (new TemplatedEmail())
        ->from($this->sender)
        ->to($account)
        ->priority(Email::PRIORITY_NORMAL)
        ->subject('Code de vérification')
        ->htmlTemplate('email/twofactor.html.twig')
        ->context([
            'company' => $_SERVER['COMPANY_NAME'],
            'code' => $code,
            'name' => $name
        ]);

        $this->mailer->send($email);
    }

    public function sendResetPass($email, $name, $resetPassword, $link){
        $email = (new TemplatedEmail())
            ->from($this->sender)
            ->to($email)
            ->priority(Email::PRIORITY_NORMAL)
            ->subject('Réinitialiser votre email')
            ->htmlTemplate('email/resetpassword.html.twig')
            ->context([
                'company' => $_SERVER['COMPANY_NAME'],
                'supportEmail' => $_SERVER['COMPANY_EMAIL'],
                'name' => $name,
                'link' => $link . 'password?hash=' . $resetPassword->getResetkey() . '&u=' . $resetPassword->getSelector()
            ]);
    
            
        $this->mailer->send($email);
    }

    public function sendMail(){

        $sender = new Address('alinassirou84@gmail.com', 'Nassirou from Abidjan Habitât');

        $email = (new Email())
            ->from($sender)
            ->to('somuchapp@gmail.com')
            //->cc('Abidjan Habitât')
            //->bcc('Abidjan Habitât')
            ->replyTo('alinassirou84@gmail.com')
            ->priority(Email::PRIORITY_NORMAL)
            ->subject('Bonjour et Bienvenue')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $this->mailer->send($email);
    }

    public function addToContact($email){
        $sg = new \SendGrid($_SERVER['SENDGRID_KEY']);
        $request_body = json_decode('{
            "contacts": [
                {
                    "email": '. $email .'
                }
            ]
        }');
        $response = $sg->client->marketing()->contacts()->put($request_body);
        print $response->statusCode() . "\n";
        print_r($response->headers());
        print $response->body() . "\n";
    }
}