<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $slugger;

    public function __construct(SluggerInterface $slugger, $targetVideoDirectory, $targetPictureDictory)
    {
        $this->slugger = $slugger;
        $this->video = $targetVideoDirectory;
        $this->picture = $targetPictureDictory;
    }

    public function upload(UploadedFile $file, $type)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename . uniqid());
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $imageTemp = $file->getPathname();
            $mime = $file->getMimeType();
            $thumbnail = 'thumbnail-' . $fileName;

            $this->compressImage($imageTemp, $this->getTargetPath($type) . '/' . $thumbnail, 25, $mime);
            $file->move($this->getTargetPath($type), $fileName);
        } catch (FileException $e) {
            $fileName = null;
            // ... handle exception if something happens during file upload
        }

        return [
            'thumnail' => $thumbnail,
            'image' => $fileName
        ];
    }

    public function getTargetPath($type){
        if($type === 'picture'){
            return $this->picture;
        }
        return $this->video;
    }

    public function compressImage($source, $destination, $quality, $mime) { 
        // Create a new image from file 
        switch($mime){ 
            case 'image/jpeg': 
                $image = imagecreatefromjpeg($source); 
                break; 
            case 'image/png': 
                $image = imagecreatefrompng($source); 
                break; 
            case 'image/gif': 
                $image = imagecreatefromgif($source); 
                break; 
            default: 
                $image = imagecreatefromjpeg($source); 
        } 
         
        // Save image 
        imagejpeg($image, $destination, $quality); 
         
        // Return compressed image 
        return $destination; 
    } 
}