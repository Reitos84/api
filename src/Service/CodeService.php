<?php

namespace App\Service;

class CodeService
{
    public static function getCode(){
        return rand(1000, 9999);
    }
}