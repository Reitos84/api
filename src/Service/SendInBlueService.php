<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mailer\Messenger\SendEmailMessage;

class SendInBlueService {

    private $mailer;
    private $sender;

    public function __construct(MailerInterface $email)
    {
        $this->mailer = $email;
        $this->sender = new Address('support@abidjanhabitat.com', 'Serge depuis Abidjan Habitât');
    }

    public function sendResetPass($email, $name, $resetPassword, $link){
        $email = (new TemplatedEmail())
            ->from($this->sender)
            ->to($email)
            ->priority(Email::PRIORITY_NORMAL)
            ->subject('Réinitialiser votre email')
            ->htmlTemplate('email/resetpassword.html.twig')
            ->context([
                'company' => $_SERVER['COMPANY_NAME'],
                'supportEmail' => $_SERVER['COMPANY_EMAIL'],
                'name' => $name,
                'link' => $link . 'password?hash=' . $resetPassword->getResetkey() . '&u=' . $resetPassword->getSelector()
            ]);
    
            
        $this->mailer->send($email);
    }
}