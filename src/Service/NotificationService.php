<?php

namespace App\Service;

use App\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;

class NotificationService {

    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function sendInterestNotifier($agency){
        $alert = new Message();
        $alert->setUser($agency)
            ->setCategory('interest')
            ->setTitle('Nouvelle intérêt')
            ->setLink($_SERVER['AGENCY_LINK'] . '/interest')
            ->setContent('Vous avez un intérêt pour votre propriété, consultez maintenant');
            
        $this->manager->persist($alert);
        $this->manager->flush();
    }

    public function registerProperty($user, $property) {
        $alert = new Message();
        $alert->setUser($user)
            ->setCategory('post')
            ->setTitle('Publication Réussie')
            ->setProperty($property)
            ->setContent('Votre propriété a maintenant disponible en ligne');
        $this->manager->persist($alert);
        $this->manager->flush();
    }

    public function sendAgencyRegister($agency){
        $alert = new Message();
        $alert->setUser($agency)
            ->setCategory('register')
            ->setTitle('Bienvenue sur Abidjan Habitât')
            ->setContent('Commencez par publier vos propriétés pour bénéficier de nombreux avantages');
        $this->manager->persist($alert);
        $this->manager->flush();
    }

    public function sendFirstPropertyRegister($agency, $property){
        $alert = new Message();
        $alert->setUser($agency)
            ->setCategory('post')
            ->setTitle('Publication Réussie')
            ->setProperty($property)
            ->setContent('Encore une étape de franchie, consultez le guide pour optimiser vos ventes');
        $this->manager->persist($alert);
        $this->manager->flush();
    }

    public function sendClientRegister($user){
        $alert = new Message();
        $alert->setUser($user)
            ->setCategory('register')
            ->setTitle('Bienvenue sur Abidjan Habitât')
            ->setContent('Vous avez besoin d\'un bien immobiler, vous êtes au bon endroit.');
        $this->manager->persist($alert);
        $this->manager->flush();
    }
}