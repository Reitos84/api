<?php

namespace App\Service;

use Twilio\Rest\Client;


final class TwilioService {

    private $client;
    private $twilioPhone;

    public function __construct(
        string $twilioSID,
        string $twilioToken,
        string $twilioPhone
    )
    {
        $this->twilioPhone = $twilioPhone;
        $this->client = new Client($twilioSID, $twilioToken);
    }

    public function sendSms(string $phoneNumber, string $message)
    {
        $this->client->messages->create(
            // the number you'd like to send the message to
            $phoneNumber,
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $this->twilioPhone,
                // the body of the text message you'd like to send
                'body' => $message
            ]
        );

        return true;
    }
}