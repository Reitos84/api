<?php

namespace App\Command;

use DateTime;
use App\Entity\Picture;
use App\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeletedPropertyCommand extends Command{

    protected static $defaultName = 'app:deleted-property-manager';
    private $manager;
    private $mailer;
    private $storage;
    private $publicPath;

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em, MailerInterface $mailer, StorageInterface $store, String $publicDir)
    {
        $this->manager = $em;
        $this->mailer = $mailer;
        $this->storage = $store;
        $this->publicPath = $publicDir;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        //Get deleted Properties
        $propertyRepository = $this->manager->getRepository(Property::class);
        $pictureRepository = $this->manager->getRepository(Picture::class);
        $deletedProperties = $propertyRepository->findBy(['status' => 'supprimer', 'deleted' => false]);
        

        //Get current time
        $now = new \DateTime();
        foreach($deletedProperties as $property){
            //Get date diff
            $date_diff = date_diff($now, $property->getDeletedAt());
            $wait_time = (int) $_SERVER['DELETED_PROPERTY_WAIT_TIME'];
            if($date_diff->days > $wait_time){
                //Delete property pictures list
                $my_pictures = $pictureRepository->findBy(['property' => $property]);
                foreach($my_pictures as $picture){
                    try{
                        unlink($this->publicPath . $this->storage->resolveUri($picture, 'file'));
                        $this->manager->remove($picture);
                    }catch(\Throwable $t){

                    }
                }
                $property->setDeleted(true);
                $this->manager->persist($property);
            }
        }
        $this->manager->flush();

        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}