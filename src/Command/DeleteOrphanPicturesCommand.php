<?php

namespace App\Command;

use DateTime;
use App\Entity\Booking;
use App\Entity\Picture;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteOrphanPicturesCommand extends Command{

    protected static $defaultName = 'app:delete-orphan-picture';
    private $manager;
    private $storage;
    private $publicPath;

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em, StorageInterface $store, String $publicDir)
    {
        $this->manager = $em;
        $this->storage = $store;
        $this->publicPath = $publicDir;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $pictureRepository = $this->manager->getRepository(Picture::class);
        $orphanPictures = $pictureRepository->findBy(['property' => null]);
        $count = 0;
        foreach($orphanPictures as $picture){
            try{
                unlink($this->publicPath . $this->storage->resolveUri($picture, 'file'));
                $this->manager->remove($picture);
                $count++;
                if($count > 25){
                    $this->manager->flush();
                }
            }catch(Exception $e){

            }
        }
        if($count > 0){
            $this->manager->flush();
        }
        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}