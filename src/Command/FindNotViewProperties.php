<?php

namespace App\Command;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class FindNotViewProperties extends Command{

    protected static $defaultName = 'app:not-viewed-property';
    private $manager;
    private $mailer;

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em, MailerInterface $mailer)
    {
        $this->manager = $em;
        $this->mailer = $mailer;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        //Put Your code here

        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}