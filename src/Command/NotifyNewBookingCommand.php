<?php

namespace App\Command;

use App\Service\SendGridMailerService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class NotifyNewBookingCommand extends Command{

    protected static $defaultName = 'app:notifiy-new-booking';
    private $manager;
    private $mailer;

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em, MailerInterface $mailer, SendGridMailerService $service)
    {
        $this->manager = $em;
        $this->mailer = $service;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        //Put Your code here
        $this->mailer->sendMail();
        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}