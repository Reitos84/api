<?php

namespace App\Command;

use App\Entity\Booking;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BookingManagerCommand extends Command{

    protected static $defaultName = 'app:manager-booking';
    private $manager;
    private $mailer;

    protected function configure(): void{
        
    }

    public function __construct(EntityManagerInterface $em, MailerInterface $mailer)
    {
        $this->manager = $em;
        $this->mailer = $mailer;
        parent::__construct();
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $bookingRepository = $this->manager->getRepository(Booking::class);
        $bookings = $bookingRepository->findBy(['status' => 0]);
        $io = new SymfonyStyle($input, $output);
        $today = new DateTime();
        $quantity = 0;
        foreach($bookings as $booking){
            if($booking->getEndsAt() < $today){
                $booking->setStatus(2);
                $booking->setReview(1);
                $this->manager->persist($booking);
                $quantity++;
            }
            if($quantity > 20){
                $this->manager->flush();
            }
        }
        $this->manager->flush();
        $io->success('La commande a été effectuer avec succès');
        return Command::SUCCESS;
    }
}