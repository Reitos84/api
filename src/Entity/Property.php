<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\AgencyOwnedInterface;
use App\Controller\AddViewOnProperty;
use App\Controller\PropertyNoteReview;
use App\Repository\PropertyRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\PreditLocationController;
use App\Controller\PropertyOnLocationController;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "normalization_context"={"groups"={"property:get", "property:get:item"}}
 *      },
 *      "put"={
 *          "denormalization_context"={"groups"={"property:put"}},
 *          "security"="is_granted('ROLE_AGENCY') || is_granted('ROLE_ADMIN')"
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_SUPER_ADMIN')"
 *      },
 *      "view"={
 *          "path"="/property/view/{id}",
 *          "method"="get",
 *          "controller"=AddViewOnProperty::class,
 *          "openapi_context"={
 *              "summary"="Ajouter une vue à une maison",
 *              "example"="true"
 *          }
 *      },
 *      "reviews"={
 *          "path"="/property/review/{id}",
 *          "method"="get",
 *          "controller"=PropertyNoteReview::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Recupère le résumé des avis des clients",
 *              "example"="true"
 *          }
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "pagination_items_per_page"=20,
 *          "normalization_context"={"groups"={"property:get", "property:get:collection"}}
 *      },
 *      "post"={
 *          "denormalization_context"={"groups"={"property:post"}},
 *          "security"="is_granted('ROLE_AGENCY')"
 *      },
 *      "preditLocation"={
 *          "path"="/properties/address",
 *          "method"="get",
 *          "controller"=PreditLocationController::class,
 *          "openapi_context"={
 *              "summary"="Get total property for different location",
 *              "description"="Dont use different parameters",
 *              "requestBody"={
 *                  "content"={
 *                      "application/json"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                              }
 *                          },
 *                          "example"={
 *                              "email"=""
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      },
 *      "getForRegion"={
 *          "path"="/properties/location",
 *          "method"="get",
 *          "controller"=PropertyOnLocationController::class,
 *          "openapi_context"={
 *              "summary"="Get total property for different location",
 *              "description"="Dont use different parameters"
 *          }
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={
 *      "agency"="exact",
 *      "category"="exact", 
 *      "area"="partial",
 *      "type"="exact", 
 *      "title"="partial", 
 *      "slug"="exact",
 *      "city"="partial",
 *      "address"="partial",
 *      "status"="exact",
 *      "deleted"="exact",
 *      "code"="exact",
 *      "bedroom"="exact",
 *      "saloon"="exact",
 *      "kitchen"="exact",
 *      "shower"="exact"
 *  }
 * )
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={
 *      "price",
 *      "createdAt"
 *  }
 * )
 * @ApiFilter(
 *  RangeFilter::class,
 *  properties={
 *      "price",
 *      "lat",
 *      "lng"
 *  }
 * )
 * @ORM\Entity(repositoryClass=PropertyRepository::class)
 */
class Property implements AgencyOwnedInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"property:get", "get:report:collection", "get:report", "booking:get", "get:saved", "get:interest", "featured:get:collection"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"property:post", "get:report:collection", "get:report", "property:put", "property:get", "booking:get", "get:saved", "get:interest", "featured:get:collection"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $area;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $country;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $frequency;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"property:post", "property:get", "get:saved", "featured:get:collection"})
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"property:post", "property:get", "get:saved", "featured:get:collection"})
     */
    private $agency;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"property:get"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "property:get", "get:saved", "featured:get:collection"})
     */
    private $what3words;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:put"})
     */
    private $deleted;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $status;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $pool;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $balcony;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $gymnasium;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $garden;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $generator;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $fountain;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $garage;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $office;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $stair;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $elevator;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $library;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $bedroom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $kitchen;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $airConditionner;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $refrigerator;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $saloon;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $shower;

    /**
     * @ORM\OneToMany(targetEntity=Picture::class, mappedBy="property", orphanRemoval=true)
     * @Groups({"property:get", "get:report:collection", "booking:get", "get:saved", "featured:get:collection", "get:interest"})
     */
    private $pictures;

    /**
     * @ORM\OneToMany(targetEntity=Featured::class, mappedBy="property", orphanRemoval=true)
     */
    private $featureds;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $buildAt;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:put", "property:get"})
     */
    private $view;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $internet;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $microwave;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $armchair;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $blender;

    /**
     * @ORM\OneToMany(targetEntity=Boost::class, mappedBy="property", orphanRemoval=true)
     */
    private $boosts;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"property:put"})
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:get", "get:saved", "featured:get:collection"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:get", "get:report:collection", "get:interest", "get:saved", "featured:get:collection"})
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $space;

    /**
     * @Groups({"property:get", "get:saved", "featured:get:collection"})
     */
    private $iSaved;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved"})
     */
    private $appartements;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved"})
     */
    private $lat;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved"})
     */
    private $lng;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyType::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"property:post", "property:get", "get:saved", "featured:get:collection"})
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $security;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $studio;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $cook;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $fan;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"property:post", "property:put", "property:get"})
     */
    private $officeType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved", "featured:get:collection"})
     */
    private $landStatus;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved"})
     */
    private $piece;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved"})
     */
    private $officePosition;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"property:post", "property:put", "property:get", "get:saved"})
     */
    private $floor;

    public function __construct(){
        $this->pictures = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->featureds = new ArrayCollection();
        $this->boosts = new ArrayCollection();
        $this->deleted = false;
        $this->createdAt = new \DateTime();
        $this->iSaved = 0;
    }

    public function getISaved(): ?int
    {
        return $this->iSaved;
    }

    public function setISaved($saved): self
    {
        $this->iSaved = $saved;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getArea(): ?string
    {
        return $this->area;
    }

    public function setArea(?string $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getFrequency(): ?string
    {
        return $this->frequency;
    }

    public function setFrequency(?string $frequency): self
    {
        $this->frequency = $frequency;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getAgency(): ?User
    {
        return $this->agency;
    }

    public function setAgency(?User $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getWhat3words(): ?string
    {
        return $this->what3words;
    }

    public function setWhat3words(?string $what3words): self
    {
        $this->what3words = $what3words;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPool(): ?bool
    {
        return $this->pool;
    }

    public function setPool(?bool $pool): self
    {
        $this->pool = $pool;

        return $this;
    }

    public function getBalcony(): ?bool
    {
        return $this->balcony;
    }

    public function setBalcony(?bool $balcony): self
    {
        $this->balcony = $balcony;

        return $this;
    }

    public function getGymnasium(): ?bool
    {
        return $this->gymnasium;
    }

    public function setGymnasium(?bool $gymnasium): self
    {
        $this->gymnasium = $gymnasium;

        return $this;
    }

    public function getGarden(): ?bool
    {
        return $this->garden;
    }

    public function setGarden(?bool $garden): self
    {
        $this->garden = $garden;

        return $this;
    }

    public function getGenerator(): ?bool
    {
        return $this->generator;
    }

    public function setGenerator(?bool $generator): self
    {
        $this->generator = $generator;

        return $this;
    }

    public function getFountain(): ?bool
    {
        return $this->fountain;
    }

    public function setFountain(?bool $fountain): self
    {
        $this->fountain = $fountain;

        return $this;
    }

    public function getGarage(): ?bool
    {
        return $this->garage;
    }

    public function setGarage(?bool $garage): self
    {
        $this->garage = $garage;

        return $this;
    }

    public function getOffice(): ?bool
    {
        return $this->office;
    }

    public function setOffice(?bool $office): self
    {
        $this->office = $office;

        return $this;
    }

    public function getStair(): ?bool
    {
        return $this->stair;
    }

    public function setStair(?bool $stair): self
    {
        $this->stair = $stair;

        return $this;
    }

    public function getElevator(): ?bool
    {
        return $this->elevator;
    }

    public function setElevator(?bool $elevator): self
    {
        $this->elevator = $elevator;

        return $this;
    }

    public function getLibrary(): ?bool
    {
        return $this->library;
    }

    public function setLibrary(?bool $library): self
    {
        $this->library = $library;

        return $this;
    }

    public function getBedroom(): ?int
    {
        return $this->bedroom;
    }

    public function setBedroom(?int $bedroom): self
    {
        $this->bedroom = $bedroom;

        return $this;
    }

    public function getKitchen(): ?int
    {
        return $this->kitchen;
    }

    public function setKitchen(?int $kitchen): self
    {
        $this->kitchen = $kitchen;

        return $this;
    }

    public function getAirConditionner(): ?bool
    {
        return $this->airConditionner;
    }

    public function setAirConditionner(?bool $airConditionner): self
    {
        $this->airConditionner = $airConditionner;

        return $this;
    }

    public function getRefrigerator(): ?bool
    {
        return $this->refrigerator;
    }

    public function setRefrigerator(?bool $refrigerator): self
    {
        $this->refrigerator = $refrigerator;

        return $this;
    }

    public function getSaloon(): ?int
    {
        return $this->saloon;
    }

    public function setSaloon(?int $saloon): self
    {
        $this->saloon = $saloon;

        return $this;
    }

    public function getShower(): ?int
    {
        return $this->shower;
    }

    public function setShower(?int $shower): self
    {
        $this->shower = $shower;

        return $this;
    }

    /**
     * @return Collection|Picture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Picture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setProperty($this);
        }

        return $this;
    }

    public function removePicture(Picture $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getProperty() === $this) {
                $picture->setProperty(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Featured[]
     */
    public function getFeatureds(): Collection
    {
        return $this->featureds;
    }

    public function addFeatured(Featured $featured): self
    {
        if (!$this->featureds->contains($featured)) {
            $this->featureds[] = $featured;
            $featured->setProperty($this);
        }

        return $this;
    }

    public function removeFeatured(Featured $featured): self
    {
        if ($this->featureds->removeElement($featured)) {
            // set the owning side to null (unless already changed)
            if ($featured->getProperty() === $this) {
                $featured->setProperty(null);
            }
        }

        return $this;
    }

    public function getBuildAt(): ?int
    {
        return $this->buildAt;
    }

    public function setBuildAt(?int $buildAt): self
    {
        $this->buildAt = $buildAt;

        return $this;
    }

    public function getView(): ?int
    {
        return $this->view;
    }

    public function setView(?int $view): self
    {
        $this->view = $view;

        return $this;
    }

    public function getInternet(): ?bool
    {
        return $this->internet;
    }

    public function setInternet(?bool $internet): self
    {
        $this->internet = $internet;

        return $this;
    }

    public function getMicrowave(): ?bool
    {
        return $this->microwave;
    }

    public function setMicrowave(?bool $microwave): self
    {
        $this->microwave = $microwave;

        return $this;
    }

    public function getArmchair(): ?bool
    {
        return $this->armchair;
    }

    public function setArmchair(?bool $armchair): self
    {
        $this->armchair = $armchair;

        return $this;
    }

    public function getBlender(): ?bool
    {
        return $this->blender;
    }

    public function setBlender(?bool $blender): self
    {
        $this->blender = $blender;

        return $this;
    }

    /**
     * @return Collection|Boost[]
     */
    public function getBoosts(): Collection
    {
        return $this->boosts;
    }

    public function addBoost(Boost $boost): self
    {
        if (!$this->boosts->contains($boost)) {
            $this->boosts[] = $boost;
            $boost->setProperty($this);
        }

        return $this;
    }

    public function removeBoost(Boost $boost): self
    {
        if ($this->boosts->removeElement($boost)) {
            // set the owning side to null (unless already changed)
            if ($boost->getProperty() === $this) {
                $boost->setProperty(null);
            }
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getSpace(): ?string
    {
        return $this->space;
    }

    public function setSpace(?string $space): self
    {
        $this->space = $space;

        return $this;
    }

    public function getAppartements(): ?int
    {
        return $this->appartements;
    }

    public function setAppartements(?int $appartements): self
    {
        $this->appartements = $appartements;

        return $this;
    }

    public function getLat(): ?float
    {
        return $this->lat;
    }

    public function setLat(?float $lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng(): ?float
    {
        return $this->lng;
    }

    public function setLng(?float $lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    public function getType(): ?PropertyType
    {
        return $this->type;
    }

    public function setType(?PropertyType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSecurity(): ?bool
    {
        return $this->security;
    }

    public function setSecurity(?bool $security): self
    {
        $this->security = $security;

        return $this;
    }

    public function getStudio(): ?bool
    {
        return $this->studio;
    }

    public function setStudio(?bool $studio): self
    {
        $this->studio = $studio;

        return $this;
    }

    public function getCook(): ?bool
    {
        return $this->cook;
    }

    public function setCook(?bool $cook): self
    {
        $this->cook = $cook;

        return $this;
    }

    public function getFan(): ?bool
    {
        return $this->fan;
    }

    public function setFan(?bool $fan): self
    {
        $this->fan = $fan;

        return $this;
    }

    public function getOfficeType(): ?string
    {
        return $this->officeType;
    }

    public function setOfficeType(?string $officeType): self
    {
        $this->officeType = $officeType;

        return $this;
    }
    
    public function getLandStatus(): ?string
    {
        return $this->landStatus;
    }

    public function setLandStatus(?string $landStatus): self
    {
        $this->landStatus = $landStatus;

        return $this;
    }

    public function getPiece(): ?int
    {
        return $this->piece;
    }

    public function setPiece(?int $piece): self
    {
        $this->piece = $piece;

        return $this;
    }

    public function getOfficePosition(): ?string
    {
        return $this->officePosition;
    }

    public function setOfficePosition(?string $officePosition): self
    {
        $this->officePosition = $officePosition;

        return $this;
    }

    public function getFloor(): ?int
    {
        return $this->floor;
    }

    public function setFloor(?int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }
}
