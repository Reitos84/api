<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\InvestorRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get",
 *      "post"={
 *          "denormalization_context"={
 *              "groups"={"investor:post"}
 *          }
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"isDone"="exact"}
 * )
 * @ORM\Entity(repositoryClass=InvestorRepository::class)
 */
class Investor
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $budget;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $calendar;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $experience;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"investor:post"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=10)
     * @Groups({"investor:post"})
     */
    private $qualify;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDone = 0;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBudget(): ?string
    {
        return $this->budget;
    }

    public function setBudget(string $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getCalendar(): ?string
    {
        return $this->calendar;
    }

    public function setCalendar(string $calendar): self
    {
        $this->calendar = $calendar;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getExperience(): ?string
    {
        return $this->experience;
    }

    public function setExperience(string $experience): self
    {
        $this->experience = $experience;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getQualify(): ?string
    {
        return $this->qualify;
    }

    public function setQualify(string $qualify): self
    {
        $this->qualify = $qualify;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(?bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }
}
