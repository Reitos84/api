<?php

namespace App\Entity;

use App\Controller\SendPicture;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\PictureRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "normalization_context"={"groups"={"picture:read"}}
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_AGENCY') or is_granted('ROLE_ADMIN')",
 *          "denormalization_context"={"groups"={"picture:edit"}}
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_AGENCY') or is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "normalization_context"={"groups"={"picture:read"}}
 *      },
 *      "send"={
 *          "pagination_enabled"=false,
 *          "path"="/pictures/send",
 *          "method"="post",
 *          "deserialize"=false,
 *          "controller"=SendPicture::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Create a new picture object",
 *              "requestBody"={ 
 *                  "content"={
 *                      "multipart/form-data"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                                   "file"={
 *                                       "type"="string",
 *                                       "format"="binary"
 *                                  },
 *                                  "isBackground"={
 *                                       "type"="boolean",
 *                                       "format"="boolean"
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      }
 *  }
 * )
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass=PictureRepository::class)
 */
class Picture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer") 
     * @Groups({"picture:read", "get:report:collection", "get:report", "booking:get", "property:get", "get:saved", "get:interest", "featured:get:collection"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="pictures")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"picture:edit"})
     */
    private $property;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"picture:read", "get:report:collection", "get:report", "picture:write", "property:get", "booking:get", "get:saved", "get:interest", "featured:get:collection"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"picture:read", "get:report:collection", "get:report", "picture:edit", "property:get", "booking:get", "get:saved", "featured:get:collection"})
     */
    private $isBackground;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="property_picture", fileNameProperty="name")
     * @Groups({"picture:write"})
     */
    private $file;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"picture:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"picture:edit"})
     */
    private $updatedAt;

    /**
     * @var string|null
     * @Groups({"picture:read", "get:report:collection", "get:report", "property:get", "booking:get", "get:saved", "get:interest", "featured:get:collection"})
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"picture:read", "get:report:collection", "property:get"})
     */
    private $thumbnail;

    public function __construct()
    {
        $this->createdAt = new \DateTime();   
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsBackground(): ?bool
    {
        return $this->isBackground;
    }

    public function setIsBackground(?bool $isBackground): self
    {
        $this->isBackground = $isBackground;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of file
     *
     * @return  File|null
     */ 
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of file
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return  self
     */ 
    public function setFile(File $file = null)
    {
        $this->file = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     *
     * @return  string|null
     */ 
    public function getUrl()
    {
        return $this->url;
    }

    /**
     *
     * @param  string|null  $avatarUrl
     *
     * @return  self
     */ 
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }
}
