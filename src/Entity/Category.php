<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CategoryRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @ApiResource(
 *  normalizationContext={"groups"={"property:get"}},
 *  itemOperations={
 *      "get",
 *      "put"={
 *          "security"="is_granted('ROLE_SUPER_ADMIN')"
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_SUPER_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "pagination_enabled"=false,
 *          "normalization_context"={"groups"={"category:get"}}
 *      },
 *      "post"={"security"="is_granted('ROLE_SUPER_ADMIN')"}
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"title"="partial", "slug"="exact"}
 * )
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"category:get", "property:get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"property:get", "category:get", "featured:get:collection"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"category:get"})
     */
    private $icon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"category:get"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"property:get", "category:get", "featured:get:collection"})
     */
    private $slug;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
