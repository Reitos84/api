<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GestureRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get",
 *      "put"={
 *          "denormalization_context"={"groups"={"gesture:edit"}}
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "normalization_context"={
 *              "groups"={"gesture:get"}
 *          }
 *      },
 *      "post"={
 *          "denormalization_context"={"groups"={"gesture:add"}}
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={
 *      "isDone"="exact"
 *  }
 * )
 * @ORM\Entity(repositoryClass=GestureRepository::class)
 */
class Gesture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"gesture:get"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"gesture:get"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=14)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $contact;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"gesture:get", "gesture:edit"})
     */
    private $isDone;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyType::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $location;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $address;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"gesture:add", "gesture:get"})
     */
    private $comment;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isDone = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(?bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getType(): ?PropertyType
    {
        return $this->type;
    }

    public function setType(?PropertyType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
