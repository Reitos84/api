<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PropertyReportRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "put",
 *      "get"={
 *           "normalization_context"={"groups"={"get:report"}},
 *           "pagination_items_per_page"=30
 *      }
 *  },
 *  collectionOperations={
 *      "post"={
 *          "denormalization_context"={
 *              "groups"={"add:report"}
 *          }
 *      },
 *      "get"={
 *          "normalization_context"={"groups"={"get:report:collection"}}
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={
 *      "isReal"="exact", 
 *      "isDone"="exact",
 *      "agency"="exact"
 *  }
 * )
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={
 *      "isReal"
 *  }
 * )
 * @ORM\Entity(repositoryClass=PropertyReportRepository::class)
 */
class PropertyReport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:report", "get:report:collection"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $property;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $role;

    /**
     * @ORM\Column(type="text")
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $provide;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get:report", "get:report:collection"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get:report", "get:report:collection"})
     */
    private $isDone;

    /**
     * @ORM\Column(type="array")
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $message = [];

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get:report", "get:report:collection"})
     */
    private $isReal;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"add:report", "get:report", "get:report:collection"})
     */
    private $agency;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isDone = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProvide(): ?bool
    {
        return $this->provide;
    }

    public function setProvide(?bool $provide): self
    {
        $this->provide = $provide;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(?bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getMessage(): ?array
    {
        return $this->message;
    }

    public function setMessage(array $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getIsReal(): ?bool
    {
        return $this->isReal;
    }

    public function setIsReal(?bool $isReal): self
    {
        $this->isReal = $isReal;

        return $this;
    }

    public function getAgency(): ?User
    {
        return $this->agency;
    }

    public function setAgency(?User $agency): self
    {
        $this->agency = $agency;

        return $this;
    }
}
