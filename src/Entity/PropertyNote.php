<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Repository\PropertyNoteRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "normalization_context"={
 *              "groups"={"get:propertyNote"}
 *          }
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_USER')",
 *          "denormalization_context"={
 *              "groups"={"edit:propertyNote"}
 *          }
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_USER')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "normalization_context"={
 *              "groups"={"get:propertyNote"}
 *          }
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_USER')",
 *          "denormalization_context"={
 *              "groups"={"add:propertyNote"}
 *          }
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"moderate"="exact", "property"="exact", "client"="exact"}
 * )
 * @ORM\Entity(repositoryClass=PropertyNoteRepository::class)
 */
class PropertyNote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:propertyNote"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"add:propertyNote"})
     */
    private $property;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"add:propertyNote"})
     */
    private $client;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"add:propertyNote", "edit:propertyNote", "get:propertyNote"})
     */
    private $note;

    /**
     * @ORM\Column(type="text")
     * @Groups({"add:propertyNote", "edit:propertyNote", "get:propertyNote"})
     */
    private $comment;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get:propertyNote"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"edit:propertyNote"})
     */
    private $isModerate;

    public function __construct()
    {
        
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsModerate(): ?bool
    {
        return $this->isModerate;
    }

    public function setIsModerate(?bool $isModerate): self
    {
        $this->isModerate = $isModerate;

        return $this;
    }
}
