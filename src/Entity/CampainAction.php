<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Repository\CampainActionRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=CampainActionRepository::class)
 * @ApiResource(
 *  paginationEnabled=false,
 *  itemOperations={
 *      "get",
 *      "delete"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')",
 *          "denormalization_context"={"groups"={"edit:campainAction"}}
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')"
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')",
 *          "denormalization_context"={"groups"={"post:campainAction"}}
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"isActive"="exact"}
 * )
 */
class CampainAction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"post:campainAction", "edit:campainAction"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"post:campainAction", "edit:campainAction"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"edit:campainAction"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"post:campainAction"})
     */
    private $provider;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): self
    {
        $this->provider = $provider;

        return $this;
    }
}
