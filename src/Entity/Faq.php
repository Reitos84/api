<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\FaqRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  normalizationContext={"groups"={"get:faq"}},
 *  collectionOperations={
 *      "get"={
 *          "pagination_items_per_page"=20
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "denormalization_context"={"groups"={"add:Faq"}}
 *      }
 *  },
 *  itemOperations={
 *      "get",
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "denormalization_context"={"groups"={"add:Faq","update:Faq"}}
 *       },
 *      "delete"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"side"="partial", "type"="exact", "isFeatured"="partial", "question"="partial", "slug"="exact"}
 * )
 * @ORM\Entity(repositoryClass=FaqRepository::class)
 */
class Faq
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:faq"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"add:Faq", "update:Faq", "get:faq"})
     */
    private $question;

    /**
     * @ORM\Column(type="text")
     * @Groups({"add:Faq", "update:Faq", "get:faq"})
     */
    private $answer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"add:Faq", "update:Faq", "get:faq"})
     */
    private $isFeatured;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get:faq"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:faq"})
     */
    private $slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"add:Faq", "update:Faq", "get:faq"})
     */
    private $isAgency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"add:Faq", "update:Faq", "get:faq"})
     */
    private $side;

    /**
     * @ORM\ManyToOne(targetEntity=FaqCategory::class)
     * @Groups({"add:Faq", "update:Faq", "get:faq"})
     */
    private $type;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function getIsFeatured(): ?bool
    {
        return $this->isFeatured;
    }

    public function setIsFeatured(?bool $isFeatured): self
    {
        $this->isFeatured = $isFeatured;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIsAgency(): ?bool
    {
        return $this->isAgency;
    }

    public function setIsAgency(?bool $isAgency): self
    {
        $this->isAgency = $isAgency;

        return $this;
    }

    public function getSide(): ?string
    {
        return $this->side;
    }

    public function setSide(?string $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getType(): ?FaqCategory
    {
        return $this->type;
    }

    public function setType(?FaqCategory $type): self
    {
        $this->type = $type;

        return $this;
    }
}
