<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PromoteRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"
 *  },
 *  collectionOperations={
 *      "get",
 *      "post"={
 *          "denormalization_context"={"groups"={"promote:post"}},
 *          "security"="is_granted('ROLE_AGENCY')"
 *      }
 *  }
 * )
 * @ORM\Entity(repositoryClass=PromoteRepository::class)
 */
class Promote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"promote:post"})
     */
    private $amount;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"promote:post"})
     */
    private $duration;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"promote:post"})
     */
    private $emailing;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"promote:post"})
     */
    private $featured;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"promote:post"})
     */
    private $social;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"promote:post"})
     */
    private $agency;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"promote:post"})
     */
    private $property;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $done;

    public function __construct(){
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getEmailing(): ?bool
    {
        return $this->emailing;
    }

    public function setEmailing(?bool $emailing): self
    {
        $this->emailing = $emailing;

        return $this;
    }

    public function getFeatured(): ?bool
    {
        return $this->featured;
    }

    public function setFeatured(?bool $featured): self
    {
        $this->featured = $featured;

        return $this;
    }

    public function getSocial(): ?bool
    {
        return $this->social;
    }

    public function setSocial(?bool $social): self
    {
        $this->social = $social;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAgency(): ?User
    {
        return $this->agency;
    }

    public function setAgency(?User $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getDone(): ?bool
    {
        return $this->done;
    }

    public function setDone(?bool $done): self
    {
        $this->done = $done;

        return $this;
    }
}
