<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MessageRepository;
use App\Controller\ViewMessageController;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Controller\DeleteMessageController;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  collectionOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_USER')",
 *          "pagination_enabled"=false
 *      },
 *      "post"={
 *      },
 *      "view"={
 *          "path"="/messages/view",
 *          "method"="put",
 *          "controller"=ViewMessageController::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Set view message for current user",
 *              "example"="false"
 *          }
 *      },
 *      "delete"={
 *          "path"="/messages/delete",
 *          "method"="delete",
 *          "controller"=DeleteMessageController::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Set view message for current user",
 *              "example"="false"
 *          }
 *      }
 *  },
 *  itemOperations={
 *      "get",
 *      "put",
 *      "delete"
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"user"="exact", "isAbsolute"="exact", "isView"="exact"}
 * )
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={"createdAt"}
 * )
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isView;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAbsolute;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     */
    private $property;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isView = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsView(): ?bool
    {
        return $this->isView;
    }

    public function setIsView(?bool $isView): self
    {
        $this->isView = $isView;

        return $this;
    }

    public function getIsAbsolute(): ?bool
    {
        return $this->isAbsolute;
    }

    public function setIsAbsolute(?bool $isAbsolute): self
    {
        $this->isAbsolute = $isAbsolute;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }
}
