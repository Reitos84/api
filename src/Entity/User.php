<?php
// src/Entity/User

namespace App\Entity;

use AskInfoConfirmDashboard;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\AvatarController;
use App\Controller\ProvideController;
use App\Controller\AdminDashboardInfo;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Controller\DashboardStatController;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ChangePasswordController;
use Symfony\Component\HttpFoundation\File\File;
use App\Controller\TwoFactorVerificationController;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 * @Vich\Uploadable
 * @ApiResource(
 *  normalizationContext={"groups"={"user:read"}},
 *  itemOperations={
 *      "get",
 *      "put"={
 *          "security"="is_granted('ROLE_USER') || is_granted('ROLE_ADMIN') || is_granted('ROLE_SUPER_ADMIN')",
 *          "denormalization_context"={"groups"={"user:edit"}}
 *       },
 *      "delete"={"security"="is_granted('ROLE_SUPER_ADMIN')"}
 *  },
 *  collectionOperations={
 *      "provide"={
 *          "security"="is_granted('ROLE_USER')",
 *          "pagination_enabled"=false,
 *          "path"="/provide",
 *          "method"="get",
 *          "controller"=ProvideController::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Get a current loggged user informations",
 *              "description"="Provide user details, if user is not be logged, return a json with error"
 *          }
 *      },
 *      "user_password"={
 *          "security"="is_granted('ROLE_USER')",
 *          "pagination_enabled"=false,
 *          "path"="/change/password",
 *          "method"="post",
 *          "controller"=ChangePasswordController::class,
 *          "denormalization_context"={"groups"={"user:changepassword"}},
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Change current logged password",
 *              "description"=""
 *          }
 *      },
 *      "ask_info"={
 *          "security"="is_granted('ROLE_USER')",
 *          "pagination_enabled"=false,
 *          "path"="/asked-info",
 *          "method"="post",
 *          "controller"=AskInfoConfirmDashboard::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Toggle user asked Info",
 *              "description"="Désactiver la demande de mot de passe"
 *          }
 *      },
 *      "adminResume"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "pagination_enabled"=false,
 *          "path"="/admin/resume",
 *          "method"="get",
 *          "controller"=AdminDashboardInfo::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Only for admin, Get all stats",
 *              "description"=""
 *          }
 *      },
 *      "resume"={
 *          "security"="is_granted('ROLE_AGENCY')",
 *          "pagination_enabled"=false,
 *          "path"="/resume",
 *          "method"="get",
 *          "controller"=DashboardStatController::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Only for Agency, Get agency resume stats",
 *              "description"="Provide agency total posted properties, required a auth agency"
 *          }
 *      },
 *      "codeVerify"={
 *          "security"="is_granted('ROLE_AGENCY')",
 *          "pagination_enabled"=false,
 *          "path"="/auth/verify",
 *          "method"="post",
 *          "controller"=TwoFactorVerificationController::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Two Factor, validation verify user code",
 *              "example"="true"
 *          }
 *      },
 *      "avatar"={
 *          "security"="is_granted('ROLE_USER')",
 *          "pagination_enabled"=false,
 *          "path"="/send/avatar",
 *          "method"="post",
 *          "deserialize"=false,
 *          "controller"=AvatarController::class,
 *          "openapi_context"={
 *              "security"={"bearerAuth"={}},
 *              "summary"="Add/Update user profile picture",
 *              "description"="Add or Update user profile picture, if user is not be logged, return a json with error",
 *              "requestBody"={
 *                  "content"={
 *                      "multipart/form-data"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                                  "file"={
 *                                      "type"="string",
 *                                      "format"="binary"
 *                                  }
 *                              }
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      },
 *      "get",
 *      "post"={
 *          "denormalization_context"={"groups"={"user:write"}}
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={
 *      "roles"="partial", 
 *      "email"="exact", 
 *      "username"="exact", 
 *      "name"="partial",
 *      "isCertified"="exact",
 *      "isEnabled"="exact",
 *      "isSignAsAgency"="exact"
 *  }
 * )
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={
 *      "createdAt"
 *  }
 * )
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"user:read", "get:interest", "booking:get", "get:propertyNote", "get:propertyNote", "get:saved", "featured:get:collection", "property:get"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"property:get", "user:read", "user:edit", "get:interest", "user:write", "booking:get"})
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     *
     * @Groups({"user:read", "user:write", "user:edit"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     *
     * @Groups({"user:write", "user:edit"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:read", "user:write"})
     */
    private $username;

    /**
     * @Groups({"user:write", "user:edit", "user:changepassword"})
     * 
     * @SerializedName("password")
     * 
     * @Assert\Length(
     *  min=6,
     *  minMessage="Le mot de passe doit être supérieur ou égal à 6 caractères",
     * )
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @Groups({"user:read", "user:write", "user:edit", "booking:get", "get:interest", "get:propertyNote", "get:saved", "featured:get:collection", "property:get"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"user:read", "user:edit", "user:write", "property:get"})
     */
    private $localisation;

    /**
     * @ORM\Column(type="text", nullable=true)
     * 
     * @Groups({"user:read", "user:edit"})
     * 
     * @Assert\Length(
     *  min=60,
     *  minMessage="Veillez saisir une description valide"
     * )
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * 
     * @Groups({"user:read", "get:interest", "user:edit", "booking:get", "user:write", "property:get"})
     * 
     * @Assert\Length(
     *  min=10,
     *  minMessage="Veillez saisir un contact valide",
     *  max=14,
     *  maxMessage="Veillez saisir un contact valide"
     * )
     */
    private $mobilephone;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * 
     * @Groups({"user:read", "user:edit", "booking:get", "user:write", "property:get"})
     * 
     * @Assert\Length(
     *  min=10,
     *  minMessage="Veillez saisir un contact valide",
     *  max=10,
     *  maxMessage="Veillez saisir un contact valide"
     * )
     */
    private $officephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * 
     * @Groups({"user:read", "user:edit", "user:write"})
     * 
     * @Assert\Length(
     *  min=10,
     *  minMessage="Veillez saisir un site web valide",
     * )
     */
    private $website;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * 
     * @Groups({"user:read"})
     */
    private $createdAt;

    /**
     * @var File|null
     * @Vich\UploadableField(mapping="agency_avatar", fileNameProperty="avatar")
     * @Groups({"user:write"})
     */
    private $file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string|null
     * 
     * @Groups({"user:read", "get:propertyNote", "get:saved", "featured:get:collection", "property:get"})
     */
    private $avatarUrl;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "booking:get", "get:propertyNote", "user:write", "get:saved", "featured:get:collection", "property:get"})
     */
    private $color;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user:read", "user:edit"})
     */
    private $isCertified;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user:read", "user:edit", "user:write"})
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user:read", "user:edit"})
     */
    private $isSecured;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currentCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:edit", "user:write"})
     */
    private $provider;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user:read", "user:edit"})
     */
    private $gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"user:read", "user:edit"})
     */
    private $birthdate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user:read", "user:edit"})
     */
    private $isAsked;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $confirmCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"user:read", "user:edit", "user:write"})
     */
    private $isSignAsAgency;

    /**
     * @Groups({"user:read"})
     */
    private $propertyTotal;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->propertyTotal = 0;
        $this->isSignAsAgency = 0;
    }

    public function getPropertyTotal():?int
    {
        return $this->propertyTotal;
    }

    public function setPropertyTotal(int $total): self
    {
        $this->propertyTotal = $total;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the value of username
     *
     * @return  self
     */ 
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get the value of plainPassword
     */ 
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Set the value of plainPassword
     *
     * @return  self
     */ 
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * Get the value of username
     */ 
    public function getUsername()
    {
        return $this->username;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(?string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function sanitizePhoneNumber(string $phoneNumber)
    {
        // remove + sign if provided in string
        return str_replace('+', '', $phoneNumber);
    }

    public  function validatePhoneNumber(string $phoneNumber)
    {
        if (!preg_match('/[1-9]\d{1,14}$/', $phoneNumber)) {
            throw new \InvalidArgumentException(
                'Please provide phone number in E164 format without the \'+\' symbol'
            );
        }
    }

    public function getMobilephone(): ?string
    {
        return "+$this->mobilephone";
    }

    public function setMobilephone(?string $mobilephone): self
    {
        $sanitizedPhoneNumber = $this->sanitizePhoneNumber($mobilephone);
        $this->validatePhoneNumber($sanitizedPhoneNumber);
        $this->mobilephone = $sanitizedPhoneNumber;


        return $this;
    }

    public function getOfficephone(): ?string
    {
        return $this->officephone;
    }

    public function setOfficephone(?string $officephone): self
    {
        $this->officephone = $officephone;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get the value of file
     *
     * @return  File|null
     */ 
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set the value of file
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return  self
     */ 
    public function setFile(File $file = null)
    {
        $this->file = $file;

        if ($file) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get the value of avatarUrl
     *
     * @return  string|null
     */ 
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * Set the value of avatarUrl
     *
     * @param  string|null  $avatarUrl
     *
     * @return  self
     */ 
    public function setAvatarUrl($avatarUrl)
    {
        $this->avatarUrl = $avatarUrl;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getIsCertified(): ?bool
    {
        return $this->isCertified;
    }

    public function setIsCertified(?bool $isCertified): self
    {
        $this->isCertified = $isCertified;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getIsSecured(): ?bool
    {
        return $this->isSecured;
    }

    public function setIsSecured(?bool $isSecured): self
    {
        $this->isSecured = $isSecured;

        return $this;
    }

    public function getCurrentCode(): ?int
    {
        return $this->currentCode;
    }

    public function setCurrentCode(?int $currentCode): self
    {
        $this->currentCode = $currentCode;

        return $this;
    }

    public function getProvider(): ?string
    {
        return $this->provider;
    }

    public function setProvider(?string $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getIsAsked(): ?bool
    {
        return $this->isAsked;
    }

    public function setIsAsked(?bool $isAsked): self
    {
        $this->isAsked = $isAsked;

        return $this;
    }

    public function getConfirmCode(): ?string
    {
        return $this->confirmCode;
    }

    public function setConfirmCode(?string $confirmCode): self
    {
        $this->confirmCode = $confirmCode;

        return $this;
    }

    public function getIsSignAsAgency(): ?bool
    {
        return $this->isSignAsAgency;
    }

    public function setIsSignAsAgency(?bool $isSignAsAgency): self
    {
        $this->isSignAsAgency = $isSignAsAgency;

        return $this;
    }
}
