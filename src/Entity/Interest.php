<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\InterestRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 * @ApiResource(
 *  normalizationContext={"groups"={"get:interest"}},
 *  itemOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_USER') || is_granted('ROLE_AGENCY') || is_granted('ROLE_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_AGENCY') || is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "pagination_items_per_page"=15,
 *          "security"="is_granted('ROLE_USER') || is_granted('ROLE_AGENCY') || is_granted('ROLE_ADMIN')"
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_USER') || is_granted('ROLE_AGENCY') || is_granted('ROLE_ADMIN')"
 *      }
 *  }
 * )
 * 
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"agency"="exact", "property"="exact", "email"="exact", "done"="exact"}
 * )
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={
 *      "createdAt"
 *  }
 * )
 * @ORM\Entity(repositoryClass=InterestRepository::class)
 */
class Interest implements AgencyOwnedInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:interest"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get:interest"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get:interest"})
     */
    private $property;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get:interest"})
     */
    private $agency;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get:interest"})
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get:interest"})
     */
    private $more_details;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get:interest"})
     */
    private $visit;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:interest"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:interest"})
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:interest"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:interest"})
     */
    private $country;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get:interest"})
     */
    private $done;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOpened;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get:interest"})
     */
    private $isRemind;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get:interest"})
     */
    private $remindedAt;

    /**
     * @Groups({"get:interest"})
     */
    private $isExpired;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getAgency(): ?User
    {
        return $this->agency;
    }

    public function setAgency(?User $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getMoreDetails(): ?bool
    {
        return $this->more_details;
    }

    public function setMoreDetails(?bool $more_details): self
    {
        $this->more_details = $more_details;

        return $this;
    }

    public function getVisit(): ?bool
    {
        return $this->visit;
    }

    public function setVisit(?bool $visit): self
    {
        $this->visit = $visit;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getDone(): ?bool
    {
        return $this->done;
    }

    public function setDone(?bool $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getIsOpened(): ?bool
    {
        return $this->isOpened;
    }

    public function setIsOpened(?bool $isOpened): self
    {
        $this->isOpened = $isOpened;

        return $this;
    }

    public function getIsRemind(): ?bool
    {
        return $this->isRemind;
    }

    public function setIsRemind(?bool $isRemind): self
    {
        $this->isRemind = $isRemind;

        return $this;
    }

    public function getRemindedAt(): ?\DateTimeInterface
    {
        return $this->remindedAt;
    }

    public function setRemindedAt(?\DateTimeInterface $remindedAt): self
    {
        $this->remindedAt = $remindedAt;

        return $this;
    }

    public function getIsExpired(): ?bool
    {
        return $this->isExpired;
    }

    public function setIsExpired(bool $isExpired): self
    {
        $this->isExpired = $isExpired;

        return $this;
    }
}
