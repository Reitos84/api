<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\EstimateRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\EstimateResumeController;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "post"={
 *          "denormalization_context"={"groups"={"estimate:post"}}
 *      },
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "resume"={
 *          "path"="/estimate/resume",
 *          "method"="get",
 *          "controller"=EstimateResumeController::class,
 *          "openapi_context"={
 *              "summary"="Resume des estimation",
 *              "example"="true"
 *          }
 *      }
 *  }
 * )
 * 
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={
 *      "isDone"="exact"
 *  }
 * )
 * 
 * @ApiFilter(
 *  OrderFilter::class,
 *  properties={
 *      "isContacted",
 *      "createdAt"
 *  }
 * )
 * 
 * @ORM\Entity(repositoryClass=EstimateRepository::class)
 */
class Estimate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"estimate:post"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"estimate:post"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=14)
     * @Groups({"estimate:post"})
     */
    private $contact;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"estimate:post"})
     */
    private $city;

    /**
     * @ORM\Column(type="text")
     * @Groups({"estimate:post"})
     */
    private $localisation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDone;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isContacted;

    /**
     * @ORM\ManyToOne(targetEntity=PropertyType::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"estimate:post"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"estimate:post"})
     */
    private $address;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"estimate:post"})
     */
    private $comment;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isDone = false;
        $this->isContacted = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(?bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getIsContacted(): ?bool
    {
        return $this->isContacted;
    }

    public function setIsContacted(?bool $isContacted): self
    {
        $this->isContacted = $isContacted;

        return $this;
    }

    public function getType(): ?PropertyType
    {
        return $this->type;
    }

    public function setType(?PropertyType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
