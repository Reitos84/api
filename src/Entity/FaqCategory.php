<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\FaqCategoryRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=FaqCategoryRepository::class)
 * @ApiResource(
 *  normalizationContext={
 *      "groups"={"get:faq", "get:faq:category"}
 *  },
 *  itemOperations={
 *      "get",
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get",
 *      "post"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  }
 * )
 */
class FaqCategory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:faq", "get:faq:category"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:faq", "get:faq:category"})
     */
    private $title;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
