<?php

namespace App\Entity;

interface AgencyOwnedInterface
{
    public function getAgency(): ?User;
    public function setAgency(?User $agency);
}