<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PrestationRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=PrestationRepository::class)
 * @ApiResource(
 *  itemOperations={
 *      "delete"={
 *          "security"="is_granted('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "normalization_context"={"groups"={"prestation:get"}}
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')"
 *      }
 *  }
 * )
 */
class Prestation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"prestation:get"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"prestation:get"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
