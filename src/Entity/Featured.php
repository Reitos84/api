<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\FeaturedRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Controller\PrintFeaturedController;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get",
 *      "put"={},
 *      "delete": {
 *          "security"="is_granted('ROLE_AGENCY') or is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "post"={
 *          "security"="is_granted('ROLE_AGENCY') or is_granted('ROLE_ADMIN')"
 *      },
 *      "get"={
 *          "normalization_context"={
 *              "groups"={"featured:get:collection"}
 *          }
 *      },
 *      "addPrint"={
 *          "path"="/featureds/print",
 *          "method"="post",
 *          "controller"=PrintFeaturedController::class,
 *          "openapi_context"={
 *              "summary"="Add print to feateared",
 *              "description"="send simple array of all id"
 *          }
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"isActive"="exact","property"="exact","user"="exact"}
 * )
 * @ORM\Entity(repositoryClass=FeaturedRepository::class)
 */
class Featured
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"featured:get:collection"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class, inversedBy="featureds")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"featured:get:collection"})
     */
    private $property;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $agency;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expiredAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $isActive;

    /**
     * @ORM\Column(type="integer")
     */
    private $ammount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $view;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $print;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $click;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"featured:get:collection"})
     */
    private $twitter;

    public function __construct()
    {
        $this->createdAt = new \DateTime();   
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getAgency(): ?User
    {
        return $this->agency;
    }

    public function setAgency(?User $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getExpiredAt(): ?\DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(\DateTimeInterface $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getAmmount(): ?int
    {
        return $this->ammount;
    }

    public function setAmmount(int $ammount): self
    {
        $this->ammount = $ammount;

        return $this;
    }

    public function getView(): ?int
    {
        return $this->view;
    }

    public function setView(?int $view): self
    {
        $this->view = $view;

        return $this;
    }

    public function getPrint(): ?int
    {
        return $this->print;
    }

    public function setPrint(?int $print): self
    {
        $this->print = $print;

        return $this;
    }

    public function getClick(): ?int
    {
        return $this->click;
    }

    public function setClick(?int $click): self
    {
        $this->click = $click;

        return $this;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): self
    {
        $this->facebook = $facebook;

        return $this;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): self
    {
        $this->instagram = $instagram;

        return $this;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): self
    {
        $this->twitter = $twitter;

        return $this;
    }
}
