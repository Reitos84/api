<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CampainTypeRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=CampainTypeRepository::class)
 * @ApiResource(
 *  paginationEnabled=false,
 *  itemOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')"
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')"
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"isActive"="exact"}
 * )
 */
class CampainType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:campainType"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:campainType"})
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:campainType"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
