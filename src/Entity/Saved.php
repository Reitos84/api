<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SavedRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  paginationItemsPerPage=20,
 *  normalizationContext={"groups"={"get:saved"}},
 *  itemOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_USER')"
 *      },
 *      "delete"={
 *          "security"="is_granted('ROLE_USER')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_USER')",
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_USER')",
 *          "denormalization_context"={
 *              "groups"={"add:saved"}
 *          }
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={"client"="exact", "property"="exact"}
 * )
 * @ORM\Entity(repositoryClass=SavedRepository::class)
 */
class Saved
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:saved"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"add:saved"})
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=Property::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"add:saved", "get:saved"})
     */
    private $property;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get:saved"})
     */
    private $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?User
    {
        return $this->client;
    }

    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
