<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ResetPasswordRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ResetPasswordActionController;
use App\Controller\RegisterResetPasswordController;
use App\Controller\VerifyResetPasswordHashController;

/**
 * @ApiResource(
 *  itemOperations={
 *      
 *  },
 *  collectionOperations={
 *      "reset_password"={
 *          "pagination_enabled"=false,
 *          "path"="/reset_password",
 *          "method"="post",
 *          "controller"=RegisterResetPasswordController::class,
 *          "openapi_context"={
 *              "summary"="Request password reset process",
 *              "description"="",
 *              "requestBody"={
 *                  "content"={
 *                      "application/json"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                                  "email"={"type"="string"}
 *                              }
 *                          },
 *                          "example"={
 *                              "email"="user@example.com"
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      },
 *      "verify_hash"={
 *          "pagination_enabled"=false,
 *          "path"="/reset_password/check",
 *          "method"="post",
 *          "controller"=VerifyResetPasswordHashController::class,
 *          "openapi_context"={
 *              "summary"="Verify les clés de réinitialisation de mot de passe",
 *              "description"="",
 *              "requestBody"={
 *                  "content"={
 *                      "application/json"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                                  "resetkey"={"type"="string"},
 *                                  "selector"={"type"="string"}
 *                              }
 *                          },
 *                          "example"={
 *                              "resetkey"="kkophert65uy5styghfh5tj",
 *                              "selector"="opznjfgr596"
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      },
 *      "change_password"={
 *          "pagination_enabled"=false,
 *          "path"="/reset_password/resolve",
 *          "method"="post",
 *          "controller"=ResetPasswordActionController::class,
 *          "openapi_context"={
 *              "summary"="Application du nouveau mot de passe",
 *              "description"="",
 *              "requestBody"={
 *                  "content"={
 *                      "application/json"={
 *                          "schema"={
 *                              "type"="object",
 *                              "properties"={
 *                                  "password"={"type"="string(min=8)"},
 *                                  "resetkey"={"type"="string"},
 *                                  "selector"={"type"="string"}
 *                              }
 *                          },
 *                          "example"={
 *                              "password"="mysecretpassword",
 *                              "resetkey"="kkophert65uy5styghfh5tj",
 *                              "selector"="opznjfgr596"
 *                          }
 *                      }
 *                  }
 *              }
 *          }
 *      },
 *  }
 * )
 * @ORM\Entity(repositoryClass=ResetPasswordRepository::class)
 */
class ResetPassword
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $resetkey;

    /**
     * @ORM\OneToOne(targetEntity=User::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expiredAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $selector;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getResetkey(): ?string
    {
        return $this->resetkey;
    }

    public function setResetkey(string $resetkey): self
    {
        $this->resetkey = $resetkey;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getExpiredAt(): ?\DateTimeInterface
    {
        return $this->expiredAt;
    }

    public function setExpiredAt(\DateTimeInterface $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function getSelector(): ?string
    {
        return $this->selector;
    }

    public function setSelector(string $selector): self
    {
        $this->selector = $selector;

        return $this;
    }
}
