<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MeetupRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=MeetupRepository::class)
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN', 'ROLE_MARKETING', 'ROLE_MEETUP', 'ROLE_SUPER_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN', 'ROLE_MEETUP', 'ROLE_SUPER_ADMIN', 'ROLE_MARKETING')"
 *      },
 *      "delete"={
 *      }
 *  },
 *  collectionOperations={
 *      "post"={
 *          "denormalization_context"={"groups"={"meetup:post"}}
 *      },
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN', 'ROLE_MEETUP', 'ROLE_SUPER_ADMIN', 'ROLE_MARKETING')"
 *      }
 *  }
 * )
 */
class Meetup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"meetup:post"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"meetup:post"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=14)
     * @Groups({"meetup:post"})
     */
    private $contact;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="text")
     * @Groups({"meetup:post"})
     */
    private $reason;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $schedule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prestation;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->status = 'upcoming';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    public function getSchedule(): ?\DateTimeInterface
    {
        return $this->schedule;
    }

    public function setSchedule(?\DateTimeInterface $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getPrestation(): ?string
    {
        return $this->prestation;
    }

    public function setPrestation(?string $prestation): self
    {
        $this->prestation = $prestation;

        return $this;
    }
}
