<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CampainRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "normalization_context"={
 *              "groups"={"get:campain"}
 *          },
 *          "pagination_items_per_page"=15
 *      },
 *      "put"={
 *          "denormalization_context"={
 *              "groups"={"edit:campain"}
 *          },
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')"
 *       },
 *      "delete"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')"
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "normalization_context"={
 *              "groups"={"get:campain"}
 *          }
 *      },
 *      "post"={
 *          "security"="is_granted('ROLE_AGENCY', 'ROLE_ADMIN')",
 *          "denormalization_context"={
 *              "groups"={"post:campain"}
 *          },
 *      }
 *  }
 * )
 * @ApiFilter(
 *  SearchFilter::class,
 *  properties={
 *      "agency" = "exact",
 *      "status" = "exact"
 *  }
 * )
 * @ORM\Entity(repositoryClass=CampainRepository::class)
 */
class Campain
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get:campain"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get:campain", "post:campain", "edit:campain"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get:campain", "post:campain", "edit:campain"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"get:campain", "post:campain", "edit:campain"})
     */
    private $days;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get:campain"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get:campain", "edit:campain"})
     */
    private $background;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get:campain", "edit:campain"})
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get:campain", "edit:campain"})
     */
    private $endsAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get:campain", "edit:campain", "post:campain"})
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @Groups({"get:campain", "post:campain", "edit:campain"})
     */
    private $agency;

    /**
     * @ORM\Column(type="text")
     * @Groups({"get:campain", "post:campain", "edit:campain"})
     */
    private $goal;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDays(): ?int
    {
        return $this->days;
    }

    public function setDays(int $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getBackground(): ?string
    {
        return $this->background;
    }

    public function setBackground(?string $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getEndsAt(): ?\DateTimeInterface
    {
        return $this->endsAt;
    }

    public function setEndsAt(?\DateTimeInterface $endsAt): self
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAgency(): ?User
    {
        return $this->agency;
    }

    public function setAgency(?User $agency): self
    {
        $this->agency = $agency;

        return $this;
    }

    public function getGoal(): ?string
    {
        return $this->goal;
    }

    public function setGoal(string $goal): self
    {
        $this->goal = $goal;

        return $this;
    }
}
