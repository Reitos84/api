<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\ContactRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  itemOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "put"={
 *          "security"="is_granted('ROLE_ADMIN')",
 *          "denormalization_context"={
 *              "groups"={"contact:put"}
 *          }
 *      }
 *  },
 *  collectionOperations={
 *      "get"={
 *          "security"="is_granted('ROLE_ADMIN')"
 *      },
 *      "post"={
 *          "denormalization_context"={
 *              "groups"={"contact:post"}
 *          }
 *      }
 *  }
 * )
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 */
class Contact
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"contact:post"})
     */
    private $person;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"contact:post"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"contact:post"})
     */
    private $email;

    /**
     * @ORM\Column(type="text")
     * @Groups({"contact:post"})
     */
    private $message;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"contact:put"})
     */
    private $isDone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"contact:post"})
     */
    private $subject;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPerson(): ?string
    {
        return $this->person;
    }

    public function setPerson(string $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getIsDone(): ?bool
    {
        return $this->isDone;
    }

    public function setIsDone(?bool $isDone): self
    {
        $this->isDone = $isDone;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }
}
