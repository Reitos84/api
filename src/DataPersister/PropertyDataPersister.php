<?php
// src/DataPersister/PropertyDataPersister.php

namespace App\DataPersister;

use App\Entity\Property;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 *
 */
class PropertyDataPersister implements ContextAwareDataPersisterInterface
{
    private $_entityManager;
    private $slug;

    public function __construct(
        EntityManagerInterface $entityManager,
        SluggerInterface $slug
    ) {
        $this->_entityManager = $entityManager;
        $this->slug = $slug;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Property;
    }

    /**
     * @param Property $data
     */
    public function persist($data, array $context = [])
    {
        if(!$data->getId()){
            $code = abs(crc32(uniqid()));

            $mySlug = $this->slug->slug($data->getTitle()) . '-' . $code ;
    
            $data->setSlug(strtolower($mySlug));
            $data->setCode($code);
        }

        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}