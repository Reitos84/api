<?php

namespace App\DataPersister; 

use App\Service\SendGridMailerService;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Interest;
use App\Entity\Reminder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ReminderDataPersister implements ContextAwareDataPersisterInterface
{
    private $_entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->_entityManager = $entityManager;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Reminder;
    }
    
    public function persist($data, array $context = [])
    {
        if(!$data->getId() && $data->getType() === 'Interest'){
            $interestRepo = $this->_entityManager->getRepository(Interest::class);
            $currentInterest = $interestRepo->find($data->getIdentifier());
            if($currentInterest){
                $currentInterest->setIsRemind(true);
                $currentInterest->setRemindedAt(new \DateTime());
                $this->_entityManager->persist($currentInterest);
            }
        }
        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = []){
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}