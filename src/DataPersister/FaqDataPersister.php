<?php
// src/DataPersister/PropertyDataPersister.php

namespace App\DataPersister;

use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Faq;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 *
 */
class FaqDataPersister implements ContextAwareDataPersisterInterface
{
    private $_entityManager;
    private $slug;

    public function __construct(
        EntityManagerInterface $entityManager,
        SluggerInterface $slug
    ) {
        $this->_entityManager = $entityManager;
        $this->slug = $slug;
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof Faq;
    }

    /**
     * @param Property $data
     */
    public function persist($data, array $context = [])
    {
        if(!$data->getId()){
            $mySlug = $this->slug->slug($data->getQuestion());
            $data->setSlug(strtolower($mySlug));
        }
        $this->_entityManager->persist($data);
        $this->_entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove($data, array $context = [])
    {
        $this->_entityManager->remove($data);
        $this->_entityManager->flush();
    }
}