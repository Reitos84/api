<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\CodeService;
use App\Service\SendGridMailerService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MailerController extends AbstractController
{
    private $mailer;

    public function __construct(SendGridMailerService $mailer){
        $this->mailer = $mailer;
    }

    /**
     * @Route("api/mail/two-factor-auth/{email}")
     */
    public function twoFactorMailSend(string $email){
        $manager = $this->getDoctrine()->getManager();
        $userRepo = $manager->getRepository(User::class);
        $me = $userRepo->findOneBy(['email' => $email]);
        if($me){
            if($me->getCurrentCode()){
                $code = $me->getCurrentCode();
            }else{
                $code = CodeService::getCode();
            }
            $me->setCurrentCode($code);
            $this->mailer->twoFactor($email, $code, $me->getName());
            $manager->persist($me);
            $manager->flush();
            return $this->json([
                'code' => 200,
                'success' => true,
                'message' => 'Email envoyé'
            ]);
        }else{
            return $this->json([
                'code' => 401,
                'message' => 'No authenticated user'
            ]);
        }
    }
}