<?php

namespace App\Controller;

use App\Entity\Property;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AddViewOnProperty extends AbstractController
{
    public function __invoke(Int $id)
    {
        $manager = $this->getDoctrine()->getManager();
        $propertyRepository = $manager->getRepository(Property::class);

        $property = $propertyRepository->find($id);
        $currentView = $property->getView();
        if($currentView){
            $currentView++;
        }else{
            $currentView = 1;
        }
        $property->setView($currentView);

        $manager->persist($property);
        $manager->flush();

        return $this->json($property, 200);
    }
}