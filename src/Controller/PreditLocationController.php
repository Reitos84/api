<?php

namespace App\Controller;

use App\Entity\Property;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PreditLocationController extends AbstractController
{
    public function __invoke(Request $r)
    {
        $key = $r->get('key');
        if(strlen($key) > 0){
            $propertyRepository = $this->getDoctrine()->getManager()->getRepository(Property::class);
            $areas = $propertyRepository->preditArea($key);
        }else{
            $areas = [];
        }
        return $this->json($areas);
    }
}