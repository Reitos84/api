<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Picture;
use App\Service\FileUploader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Storage\StorageInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SendPicture extends AbstractController
{
    public function __invoke(Request $r, FileUploader $fileUploader, StorageInterface $store)
    {
        $manager = $this->getDoctrine()->getManager();
        $listReturn = [];
        foreach($r->files as $file){
            try{
                $fileArray = $fileUploader->upload($file, 'picture');
                if($fileArray){
                    $picture = new Picture();
                    $picture->setName($fileArray['thumnail']);
                    $picture->setUpdatedAt(new \DateTime());
                    $picture->setThumbnail($fileArray['image']);
                    $picture->setIsBackground($r->request->get('isBackground'));
                    $manager->persist($picture);
                    $picture->setUrl($store->resolveUri($picture, 'file'));
                    $listReturn[] = $picture;
                }
            }catch(\Throwable $t){
                continue;
            }
        }
        $manager->flush();
        return $this->json($listReturn);
    }
}
