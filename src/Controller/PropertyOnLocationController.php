<?php

namespace App\Controller;

use App\Entity\Picture;
use App\Entity\Property;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\String\Slugger\SluggerInterface;

class PropertyOnLocationController extends AbstractController
{
    public function __invoke(SluggerInterface $slugger)
    {
        $manager = $this->getDoctrine()->getManager();
        $propertyReo = $manager->getRepository(Property::class);
        $picturesRepo = $manager->getRepository(Picture::class);

        $area = $propertyReo->allAreas();
        $array_result = [];

        foreach($area as $value){
            $totalProperties = $propertyReo->count(['address' => $value[1]]);
            if($totalProperties > 0){
                $array_result[] = [
                    'area' => $value[1],
                    'total' => $totalProperties
                ];
            }
        }

        usort($array_result, function ($x, $y) {
            return $x['total'] >= $y['total'] ? -1 : 1;
        });

        if(count($array_result)){
            $firstSide = $array_result[0];
            $c_properties = $propertyReo->findBy(['address' => $firstSide['area']]);
            shuffle($c_properties);
            $c_property = $c_properties[0];
            $pictures = $picturesRepo->findBy(['property' => $c_property]);
            $indexedPictures = $pictures[0];
            foreach($pictures as $p){
                if($p->getIsBackground()){
                    $indexedPictures = $p;
                }
            }
            $firstSide['image'] = '/images/property/' . $indexedPictures->getName();
            $array_result[0] = $firstSide;
        }

        return $this->json($array_result);
    }
}