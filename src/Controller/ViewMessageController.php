<?php

namespace App\Controller;

use App\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ViewMessageController extends AbstractController
{
    public function __invoke()
    {
        $manager = $this->getDoctrine()->getManager();
        $messageRepository = $manager->getRepository(Message::class);
        if($this->getUser()){
            $messageRepository->setSeen($this->getUser()->getId());
        }
        return $this->json([
            'success' => true,
            'message' => 'ok'
        ]);
    }
}