<?php

namespace App\Controller;

use App\Entity\Featured;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PrintFeaturedController extends AbstractController
{
    public function __invoke(Request $r)
    {
        $content = json_decode($r->getContent());
        $manager = $this->getDoctrine()->getManager();
        $featuredRepo = $manager->getRepository(Featured::class);

        foreach($content as $c){
            $me = $featuredRepo->find($c);
            $print = $me->getPrint();
            if($print){
                $print = $print + 1;
            }else{
                $print = 1;
            }
            $me->setPrint($print);
            $manager->persist($me);
        }

        $manager->flush();

        return $this->json(
            [
                'success' => true,
                'message' => 'ok'
            ]
        );
    }
}
