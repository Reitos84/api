<?php

namespace App\Controller;

use App\Entity\Estimate;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EstimateResumeController extends AbstractController
{
    public function __invoke()
    {
        $manager = $this->getDoctrine()->getManager();
        $repo = $manager->getRepository(Estimate::class);
        $all = $repo->count([]);
        $inCorse = $repo->count(['isContacted' => true]);
        $isDone = $repo->count(['isDone' => true]);

        return $this->json([
            'all' => $all,
            'inCorse' => $inCorse,
            'isDone' => $isDone
        ]);
    }
}