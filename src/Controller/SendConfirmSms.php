<?php

namespace App\Controller;

use App\Service\CodeService;
use App\Service\TwilioService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SendConfirmSms extends AbstractController{


    public function sanitizePhoneNumber(string $phoneNumber)
    {
        // remove + sign if provided in string
        return str_replace('+', '', $phoneNumber);
    }

    public  function validatePhoneNumber(string $phoneNumber)
    {
        if (!preg_match('/[1-9]\d{1,14}$/', $phoneNumber)) {
            throw new \InvalidArgumentException(
                'Please provide phone number in E164 format without the \'+\' symbol'
            );
        }
    }

    /**
     * @Route("api/send/confirm")
     */
    public function sendConfirm(Request $r, TwilioService $twilioService){

        $contact = trim($r->query->get('contact'));

        $contact = $this->sanitizePhoneNumber($contact);
        $this->validatePhoneNumber($contact);
        $manager = $this->getDoctrine()->getManager();
        $codeService = new CodeService();

        $me = $this->getUser();

        $code = '';

        $code = $codeService->getCode();

        $text = "Abidjan Habitat: votre code est $code";

        
        if($twilioService->sendSms($contact, $text)){
            $me->setConfirmCode($code);
            $manager->persist($me);
            $manager->flush();
            return $this->json([
                'success' => true
            ]);
        }else{
            return $this->json([
                'success' => false,
                'message' => 'Cannot send sms'
            ]);
        }
    }

    /**
     * @Route("api/verify/code/{code}")
     */
    public function confirmCode(Int $code){

        $manager = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        if($code == $user->getConfirmCode()){

            $user->setConfirmCode(null);
            $manager->persist($user);
            $manager->flush();
            
            return $this->json([
                'success' => true
            ]);
        }else{
            return $this->json([
                'success' => false
            ]);
        }
    }
}