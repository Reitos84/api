<?php

namespace App\Controller;

use App\Service\VonageService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DefaultController extends AbstractController{

    /**
     * @Route("/template")
     */
    public function showPage(){
        
        /*
        return $this->render('email/resetpassword.html.twig', [
            'company' => $_SERVER['COMPANY_NAME'],
            'name' => 'Addoha',
            'isAgency' => false,
            'supportEmail' => $_SERVER['COMPANY_EMAIL'],
            'link' => $_SERVER['CLIENT_LINK']
        ]);
        */
        // return $this->render('email/agencyRequest.html.twig', [
        //     'name' => 'Addoha',
        //     'agencylink' => $_SERVER['AGENCY_LINK'],
        //     'supportEmail' => $_SERVER['COMPANY_EMAIL'],
        // ]);
        return $this->render('email/acceptAgency.html.twig', [
            'name' => 'Addoha',
            'email' => 'reitos@gmail.com',
            "password" => 'reitosymg',
            'agencylink' => $_SERVER['AGENCY_LINK'],
            'supportEmail' => $_SERVER['COMPANY_EMAIL'],
        ]);
        // return $this->render('email/interest.html.twig', [
        //     'name' => 'Addoha',
        //     'agencylink' => $_SERVER['AGENCY_LINK']
        // ]);
        /*
        return $this->render('email/clientRegisterNew.html.twig', [
            'company' => $_SERVER['COMPANY_NAME'],
            'name' => 'Addoha',
            'isAgency' => false,
            'sitelink' => $_SERVER['CLIENT_LINK'],
            'agencylink' => $_SERVER['AGENCY_LINK']
        ]);
        */
    }

    /**
     * @Route("/api/countries")
     */
    public function countries(ParameterBagInterface $parameter){

        $countries = file_get_contents($parameter->get('kernel.project_dir') . '/public/data/world.json');
        

        return $this->json(
            json_decode($countries)
        );
    }
}