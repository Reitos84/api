<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Featured;
use App\Entity\Interest;
use App\Entity\Property;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardStatController extends AbstractController
{
    public function __invoke()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $categories = $em->getRepository(Category::class)->findAll();
        $allInterest = $em->getRepository(Interest::class)->count(['agency' => $user, 'done' => 0]);

        $datas = [];
        foreach($categories as $category){
            $datas[$category->getSlug()] = array(
                'label' => $category->getTitle(),
                'value' => $em->getRepository(Property::class)->count(['agency' => $user, 'deleted' => 0, 'category' => $category])
            );
        }

        $datas['interest'] = $allInterest;

        return $this->json([
            'code' => 200,
            'success' => true,
            'data' => $datas
        ]);
    }
}
