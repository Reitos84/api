<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProvideController extends AbstractController
{
    public function __invoke()
    {
        $user = $this->getUser();
        
        if($user){
            return $this->json($user);
        }

        return $this->json([
            'code' => 200,
            'success' => false,
            'message' => 'Vous n\'êtes pas connecter'
        ]);
    }
}
