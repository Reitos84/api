<?php

namespace App\Controller;

use App\Entity\ResetPassword;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class VerifyResetPasswordHashController extends AbstractController{

    public function __invoke(Request $r)
    {
        $manager = $this->getDoctrine()->getManager();
        $repository = $manager->getRepository(ResetPassword::class);

        $data = json_decode($r->getContent(), true);
        $resetKey = $data['resetkey'];
        $selector = $data['selector'];

        $object = $repository->findOneBy(['resetkey' => $resetKey, 'selector' => $selector]);

        if($object){
            $now = new \DateTime();
            if($now < $object->getExpiredAt()){
                $message = [
                    'success' => true,
                    'message' => 'lien valide'
                ];
            }else{
                $message = [
                    'success' => false,
                    'message' => 'Votre lien n\'est plus valide'
                ];
            }
        }else{
            $message = [
                'success' => false,
                'message' => 'Votre lien n\'est plus valide'
            ];
        }

        return $this->json($message);
    }
}