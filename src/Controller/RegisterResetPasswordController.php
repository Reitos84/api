<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\ResetPassword;
use App\Service\SendGridMailerService;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RegisterResetPasswordController extends AbstractController{
    
    public function __invoke(Request $r, SendGridMailerService $mailer)
    {
        $manager = $this->getDoctrine()->getManager();
        $userRepository = $manager->getRepository(User::class);
        $resetPasswordRepo = $manager->getRepository(ResetPassword::class);

        $resetPassword = new ResetPassword();

        $data = json_decode($r->getContent(), true);
        $email = $data['email'];
        $request_side = $data['side'];

        $user = $userRepository->findOneBy(['email' => $email]);
        if($user && $user->getIsEnabled()){
            if(!$user->getProvider()){

                $asResetKey = $resetPasswordRepo->findOneBy(['user' => $user]);

                $create = true;

                if($asResetKey){
                    $now = new DateTime();
                    $diff = date_diff($now, $asResetKey->getExpiredAt());
                    
                    if($diff->i < 5 or ($now > $asResetKey->getExpiredAt())){
                        $create = true;
                        $asResetKey->setUser(new User());
                        $manager->remove($asResetKey);
                        $manager->flush();
                    }else{
                        $create = false;
                        $resetPassword = $asResetKey;
                    }
                }
        
                if($create){
                    $selector = uniqid();
                    $hash = md5($selector);
                    $resetPassword->setSelector($selector)->setResetkey($hash)->setUser($user);
                    $expired = new DateTime();
                    date_modify($expired, '+30 minutes');
                    $resetPassword->setExpiredAt($expired);
                    $manager->persist($resetPassword);
                }

                if(in_array('ROLE_AGENCY', $user->getRoles()) && $request_side === 'agency'){
                    $link = $_SERVER['AGENCY_LINK'];
        
                    $mailer->sendResetPass($user->getEmail(), $user->getName(), $resetPassword, $link);
            
                    $manager->flush();
                    $message = [
                        'success' => true,
                        'message' => 'Email envoyé'
                    ];
                }else if(!in_array('ROLE_AGENCY', $user->getRoles()) && $request_side === 'client'){
                    $link = $_SERVER['CLIENT_LINK'] ;
        
                    $mailer->sendResetPass($user->getEmail(), $user->getName(), $resetPassword, $link);
            
                    $manager->flush();
                    $message = [
                        'success' => true,
                        'message' => 'Email envoyé'
                    ];
                }else{
                    $message = [
                        'success' => false,
                        'error_code' => '0x20',
                        'message' => 'Cet email n\'est pas associé à un compte'
                    ];
                }
            }else{
                $message = [
                    'success' => false,
                    'error_code' => '0x21',
                    'message' => 'Utilisez la connexion par Google ou Facebook'
                ];
            }
        }else{
            $message = [
                'success' => false,
                'error_code' => '0x20',
                'message' => 'Cet email n\'est pas associé à un compte'
            ];
        }
        return $this->json($message);
    }
}