<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ChangePasswordController extends AbstractController
{
    public function __invoke($data, UserPasswordEncoderInterface $encoder)
    {
        $manager = $this->getDoctrine()->getManager();
        $me = $this->getUser();

        $newpassword = $data->getPlainPassword();

        $me->setPassword(
            $encoder->encodePassword(
                $me,
                $newpassword
            )
        );

        $manager->persist($me);
        $manager->flush();

        return $this->json([
            "code" => 200,
            "success" => true
        ]);
    }
}
