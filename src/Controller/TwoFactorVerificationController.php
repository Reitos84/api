<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class TwoFactorVerificationController extends AbstractController
{
    public function __invoke(Request $r)
    {
        $user = $this->getUser();
        $data = json_decode($r->getContent(), true);
        $code = $data['code'];
        $manager = $this->getDoctrine()->getManager();
        
        if($user->getCurrentCode() === (int) $code){
            $user->setCurrentCode(null);
            $manager->persist($user);
            $manager->flush();
            return $this->json([
                'code' => 200,
                'success' => true
            ]);
        }else{
            return $this->json([
                'code' => 200,
                'success' => false
            ]);
        }
    }
}