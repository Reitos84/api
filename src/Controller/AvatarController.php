<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AvatarController extends AbstractController
{
    public function __invoke(Request $r)
    {
        $me = $this->getUser();
        $me->setFile($r->files->get('avatar'));
        $me->setUpdatedAt(new \DateTime());

        return $me;
    }
}
