<?php

namespace App\Controller;

use App\Entity\Campain;
use App\Entity\Property;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminDashboardInfo extends AbstractController
{
    public function __invoke()
    {
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $propertyRepository = $em->getRepository(Property::class);
        $campainRepository = $em->getRepository(Campain::class);

        $property_stat = [
            'all' => $propertyRepository->count([]),
            'online' => $propertyRepository->count(['status'=>'online']),
            'pending' => $propertyRepository->count(['status'=>'pending'])
        ];

        $campagne_stat = [
            'all' => $campainRepository->count([]),
            'online' => $campainRepository->count(['status'=>'online']),
            'pending' => $campainRepository->count(['status'=>'pending'])
        ];

        $client_stat = [
            'client' => count($userRepository->getUserWithRoles('ROLE_CLIENT')),
            'agency' => count($userRepository->getUserWithRoles('ROLE_AGENCY')),
            'certified' => $userRepository->count(['isCertified' => 1]),
            'request' => $userRepository->count(['isEnabled' => 0])
        ];

        return $this->json([
            'code' => 200,
            'property' => $property_stat,
            'campagne' => $campagne_stat,
            'user' => $client_stat
        ]);
    }
}