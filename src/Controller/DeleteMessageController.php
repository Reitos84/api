<?php

namespace App\Controller;

use App\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DeleteMessageController extends AbstractController
{
    public function __invoke()
    {
        $manager = $this->getDoctrine()->getManager();
        $messageRepository = $manager->getRepository(Message::class);
        $myNotes = $messageRepository->findBy(['user' => $this->getUser()]);

        foreach($myNotes as $note){
            $manager->remove($note);
        }

        $manager->flush();

        return $this->json([
            'success' => true,
            'message' => 'delete'
        ]);
    }
}