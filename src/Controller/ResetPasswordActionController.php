<?php

namespace App\Controller;

use App\Entity\ResetPassword;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ResetPasswordActionController extends AbstractController{

    public function __invoke(Request $r, UserPasswordEncoderInterface $encoder)
    {
        $manager = $this->getDoctrine()->getManager();
        $repository = $manager->getRepository(ResetPassword::class);

        $data = json_decode($r->getContent(), true);
        $resetKey = $data['resetkey'];
        $selector = $data['selector'];
        $password = $data['password'];

        $object = $repository->findOneBy(['resetkey' => $resetKey, 'selector' => $selector]);

        $me = $object->getUser();

        $manager->remove($object);
        
        $me->setPassword(
            $encoder->encodePassword(
                $me,
                $password
            )
        );

        $manager->persist($me);


        $manager->flush();

        return $this->json([
            "code" => 200,
            "success" => true
        ]);
    }
}