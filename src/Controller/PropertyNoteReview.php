<?php

namespace App\Controller;

use App\Entity\PropertyNote;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PropertyNoteReview extends AbstractController {

    public function __invoke(Int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $propertyNoteRepo = $em->getRepository(PropertyNote::class);

        $rate = round((float) $propertyNoteRepo->getRate($id), 1);
        $total = $propertyNoteRepo->count(['isModerate' => 1, 'property' => $id]);

        $one_star = $propertyNoteRepo->count(['isModerate' => 1, 'property' => $id, 'note' => 1]);
        $two_star = $propertyNoteRepo->count(['isModerate' => 1, 'property' => $id, 'note' => 2]);
        $three_star = $propertyNoteRepo->count(['isModerate' => 1, 'property' => $id, 'note' => 3]);
        $four_star = $propertyNoteRepo->count(['isModerate' => 1, 'property' => $id, 'note' => 4]);
        $five_star = $propertyNoteRepo->count(['isModerate' => 1, 'property' => $id, 'note' => 5]);

        return $this->json([
            'rate' => $rate,
            'total' => $total,
            'one'=> [
                'total' => $one_star,
                'percent' => round(($one_star * 100) / $total, 0)
            ],
            'two' => [
                'total' => $two_star,
                'percent' => round(($two_star * 100) / $total, 0)
            ],
            'three' => [
                'total' => $three_star,
                'percent' => round(($three_star * 100) / $total, 0)
            ],
            'four' => [
                'total' => $four_star,
                'percent' => round(($four_star * 100) / $total, 0)
            ],
            'five' => [
                'total' => $five_star,
                'percent' => round(($five_star * 100) / $total, 0)
            ]
        ]);
    }

}